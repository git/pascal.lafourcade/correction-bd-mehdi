<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8"><style><?php global $rep,$vues,$css; include $rep.$css['bootstrap']; ?></style>
        <title></title>
        <script src="css/jquery-3.5.1.js"></script>
        <link rel="stylesheet" href="css/VuePrincipale.css">
        
    </head>
    <?php 
      $tp_str = implode(",", array_keys($dVueQuestions));
      require_once('BDD/SqliteDb.php');
        $db = new SqliteDb();
        $dateVerif = array();
      //die($tp_str);
        
        
   
    ?>
    
    <body id="vuep">
        <input type="text" id='tp_keys' value="<?php echo $tp_str; ?>" hidden>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="mainNav">
           <div class="container">
             <a class="navbar-brand js-scroll-trigger" href="#page-top">Base De Données</a>
             <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
             </button>
             <div class="collapse navbar-collapse" id="navbarResponsive">
               <ul class="navbar-nav ml-auto">
                   <?php            foreach ($dVueQuestions as $tp => $question){ 
                                        $numTp  = str_replace('tp', '', $tp);
                                        $q="SELECT * FROM dateTp WHERE numTp=?";
                                        $prp=$db->prepare($q);      
                                        $prp->bindParam(1,  $numTp);
                                        $result = $prp->execute();
                                        $row = $result->fetchArray();
                                        $date = date('Ymd');
                                        if($date < $row['dateDebut'] || $date>$row['dateFin']) {
                                            $dateVerif[$tp] = 0;
                                            if($_SESSION['typeSession'] != 'LJLf1') continue;
                                        } else $dateVerif[$tp] = 1;
                                                ?>
                 <li class="nav-item">
                     <a style = "text-transform:uppercase;" class="nav-link js-scroll-trigger" id="<?php echo $tp; ?>" onclick="changerSection('<?php echo $tp;?>','<?php echo $tp_str ;?>')" ><?php echo $tp; ?></a>
                 </li>                
                   <?php }?>
                 <li>
                     <a class="nav-link js-scroll-trigger">Se connecter</a>
                 </li>
               </ul>
             </div>
           </div>
         </nav>
        <header class="bg-primary text-white">
           <div class="container text-center">
                <h1>TP</h1>
                <p class="lead"></p>
            </div>
        </header>
<div id='allp'>
    <div style="font-weight: bold" id="Temps-Restant" hidden></div>
        <form method="get" class="adm" >
            <input id="btn-ajout" type="submit" name="action"  value="Ajouter une question" />
            <input id="inp-ret" type="submit" name="action" value="Retour au menu principal" >
        </form>
        <?php         foreach ($dVueQuestions as $tp => $questions){ //die(print_r($questions)); 
                            if($dateVerif[$tp] == 0){
                                if($_SESSION['typeSession'] == 'LJLf1')
                                    $indispo = '(<i>Indisponible pour les non admin</i>)';
                                else continue;
                            }  ?>
        
        <section id="<?php echo 'o'.$tp; ?>">            
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                  <?php $numTp  = str_replace('tp', '', $tp);
                        $q="SELECT * FROM dateTp WHERE numTp=?";
                        $prp=$db->prepare($q);      
                        $prp->bindParam(1,  $numTp);
                        $result = $prp->execute();
                        $row = $result->fetchArray();
                        
                        $jdeb = substr($row['dateDebut'], 6, 2);
                        $mdeb = substr($row['dateDebut'], 4, 2);
                        $adeb = substr($row['dateDebut'], 0, 4);
                        $dateDebut = $jdeb.'-'.$mdeb.'-'.$adeb;
                        
                        $jfin = substr($row['dateFin'], 6, 2);
                        $mfin = substr($row['dateFin'], 4, 2);
                        $afin = substr($row['dateFin'], 0, 4);
                        $dateFin = $jfin.'-'.$mfin.'-'.$afin;
                  
                  ?>
                <h2 style = "text-transform:uppercase;  "  ><?php echo $tp; ?></h2><?php echo 'Disponible du '.$dateDebut.' jusqu\'au '.$dateFin.' à 00h00m01s. ';//afficher dates ?>
                <form  class="adm" style ="margin-left: 150px; ">
                    <input type="button"  value="Modifier les dates" onclick="ModifierDates(<?php echo str_replace('tp', '', $tp);?>)" />
                </form>
                
                <form  id="questionsForm<?php echo $tp ?>" method="get" name="Exam">
                        <?php $i=0; //shuffle($dVueQuestions); 
                        foreach ($questions as $q) {
                            /*if($nbQ > $_SESSION['nbQuestions']) 
                                break; */ ?>
                        <p><?php  $i++;  echo $q->getNumQuestion() .'.'.$q->getQuestion().'.'.str_repeat("&nbsp;", 8).'<i>barème : '.$q->getBareme().' point(s)</i>';  $_SESSION['num']=array();  $_SESSION['num'][$i] =$q->getNumQuestion();  ?> <br/>
                            <textarea name="question<?php echo $q->getNumQuestion(); ?>" id="question<?php echo $q->getNumQuestion().$q->getNumTp(); ?>"  rows="5" cols="70">
                                <?php  echo (isset($_SESSION['fi'][$i]) ? $_SESSION['fi'][$i] : '') ?>
                            </textarea>
                            <p><input class="submitQuestionData" type="button" value="Valider" onclick="SubmitQuestionData(<?php echo $q->getNumQuestion(); ?>,<?php echo $q->getNumTp(); ?>);" >
                                <span id="inp-centre" ><input  class="modifierQuestion adm" type="button" value="Modifier" onclick="ModifierQuestion(<?php echo $q->getNumQuestion(); ?>,<?php echo $q->getNumTp(); ?>)" >
                            <input class="supprimerQuestion adm"  type="button" value="Supprimer" onclick="SupprimerQuestion(<?php echo $q->getNumQuestion(); ?>,<?php echo $q->getNumTp(); ?>);" ></span>
                            <span id="inp-gauche" ><input  class="monterQuestion adm" type="button" value="Monter" onclick="MonterQuestion(<?php echo $q->getNumQuestion(); ?>,<?php echo $q->getNumTp(); ?>)" >
                            <input  class="descendreQuestion adm" type="button" value="Descendre" onclick="DescendreQuestion(<?php echo sizeof($questions); ?>,<?php echo $q->getNumQuestion(); ?>,<?php echo $q->getNumTp(); ?>)" ></span></p>
                        </p>
                        <div id="resultatsQuestions<?php echo $q->getNumQuestion().$q->getNumTp(); ?>">
                            <!-- les résultats s'affichent içi -->
                        </div><br/>
                        <?php $i++; } //fin foreach ?>
                </form>
              </div>
            </div>
          </div>
        </section><?php } ?>
               
        
        <form method="get" class="adm" >
            <input id="btn-ajout" type="submit" name="action"  value="Ajouter une question" />
        </form>
        <!--<a href="vues/VueAdmin.php" class="a-ajout"><button id="btn-ajout">Ajouter Une Question</button></a>
        <!--<a href="vues/VueLibre.php" class="a-ajout"><button>Creation table</button></a>-->
        
        
        
        <form method="get" role="form">
            <input id="inp-questions" class="bouton" type="submit" name="action" value="Afficher les questions" hidden>
            <input id="inp-ret" type="submit" name="action" value="Retour au menu principal" >
        </form>
        
        <form id="frm-export" class="adm">
                <input type="button" value="Exporter" onclick="ExporterTp()" />
        </form>

        <?php
        
        ?>
        <div id="resultats">
            <!-- les résultats s'affichent içi -->
        </div><br/>
        <input type="text" id="verif-ad" value="<?php echo $_SESSION['typeSession'];?>" hidden/>

</div>      
       <script type="text/javascript">                     
            var tempsMin =60 ;
            var total_secondes =60*tempsMin;
            tempsMin = parseInt(total_secondes/60);
            secondes = parseInt(total_secondes%60);
            document.getElementById("Temps-Restant").innerHTML='Temps restant: ' + tempsMin + ' minutes ' + secondes + ' secondes';
            function init(){
                document.getElementById("Temps-Restant").innerHTML='Temps restant: ' + tempsMin + ' minutes ' + secondes + ' secondes';
                setTimeout("TempsRestant()",999);
            }
            function TempsRestant(){
                document.getElementById("Temps-Restant").innerHTML='Temps restant: ' + tempsMin + ' minutes ' + secondes + ' secondes' ;
                if(total_secondes <=0){
                    setTimeout('document.Exam.submit()',1);
                } 
                else {
                    total_secondes = total_secondes -1;
                    tempsMin = parseInt(total_secondes/60);
                    secondes = parseInt(total_secondes%60);
                    setTimeout("TempsRestant()",999);
                }
            }
            init();
       
            function ExporterTp(){
                    type = 'tp';
                    $.get("Traitement/Export.php", { type:type}, function(data) {
                        alert(data);
                        alert("Les tp ont été exportées");
                    });
            }
                
            function ModifierDates(tp){
                
                $.get("Traitement/VueGestionTp.php", { tp:tp},
                function(data) {
                     $('#vuep').empty();
                     $('#vuep').html(data);
                  });
            }
       
            /*$(document).ready(function() {
                var tps = $('#tp_keys').val();
                var i = 0;
                tpk = tps.split(",");
                tpk.forEach(function(entry) {
                    if(i==0) $("#o"+entry).show();
                    else $("#o"+entry).hide();
                    i++;
                    console.log(entry);
                });                              
            });*/
            
            function changerSection(tp,keys){
                                
                tpk = keys.split(",");
                tpk.forEach(function(entry) {
                    $("#o"+entry).hide();
                    console.log(entry);
                });               
                $("#o"+tp).show();
   
            }
            
            function SubmitQuestionData(numQuestion, numTp) {
                //document.write($('#demo'+numQuestion).val());
                var txt = $('#question'+numQuestion+numTp).val() ; 

                $.get("Traitement/Correcteur.php", { textbox : txt, numQuestion : numQuestion, numTp : numTp},
                function(data) {
                     $('#resultatsQuestions'+numQuestion+numTp).html(data);
                     //$('#questionsForm')[0].reset();
                  });
                  
            } 
            
             function SupprimerQuestion(numQuestion,numTp) {
                var result = confirm("Êtes-vous sûr de vouloir supprimer la question "+numQuestion+" du TP "+numTp+" ?");
                if(!result) return;
                $.get("Traitement/Suppression.php", {  numQuestion : numQuestion, numTp : numTp},
                function(data) {
                     //$('#resultatsQuestions'+numQuestion).html(data);
                     //$('#questionsForm')[0].reset();
                        $('#inp-questions').trigger("click");
                  });
                  
            } 
            
            function ModifierQuestion(numQuestion, numTp) {
                
                $.get("Traitement/Modification.php", {  numQuestion : numQuestion, numTp : numTp },
                function(data) {
                    $('#vuep').empty();
                     $('#vuep').html(data);
                     //$('#questionsForm')[0].reset();
                     //location.reload();
                  });

            } 
          
            function MonterQuestion(numQuestion, numTp) {
                
                if(numQuestion > 1){
                    $.get("Traitement/Ordre.php", {  numQuestion : numQuestion, numTp : numTp },
                    function(data) {

                         //$('#questionsForm')[0].reset();
                        $('#inp-questions').trigger("click");
                         //window.location.replace("index.php");
                      });
                }
            }
            
            function DescendreQuestion(nbQuestions,numQuestion, numTp) {
                //alert(nbQuestions);
                
                var des = 1;
                if(numQuestion < nbQuestions){
                    $.get("Traitement/Ordre.php", { des : des , numQuestion : numQuestion, numTp : numTp },
                    function(data) {

                         //$('#questionsForm')[0].reset();
                        $('#inp-questions').trigger("click");
                         //window.location.replace("index.php");
                      });
                }
            }
            
            window.addEventListener('scroll',function() {       
                 localStorage.setItem('scrollPosition',window.scrollY);
             },false); 
             window.addEventListener('load',function() {
                 if(localStorage.getItem('scrollPosition') !== null)
                 window.scrollTo(0, localStorage.getItem('scrollPosition'));
             },false);
             
             
            $( document ).ready(function() {
                var ad = $('#verif-ad').val();
                             
                if(ad == 'LJLf1')
                    $( ".adm").show();
                else $( ".adm").remove();
                
            });
        </script>
        <!-- Footer -->
        <footer class="py-5 bg-dark">
          <div class="container">
            <p class="m-0 text-center text-white">IUT Clermont-Ferrand 2020</p>
          </div>
          <!-- /.container -->
        </footer>

        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom JavaScript for this theme -->
        <script src="js/scrolling-nav.js"></script>
         
    </body>
</html>
