 
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script src="css/jquery-3.5.1.js"></script>
        
        <link rel="stylesheet" href="css/VueAdmin.css">
    </head>
    
    <?php require_once('BDD/SqliteDb.php');
            $db = new SqliteDb();

        $reponseQuery = 'SELECT count(*) FROM QcmCorrection WHERE numQuestion=? ';
        $stmt = $db->prepare($reponseQuery);
        $stmt->bindParam(1, $q['numQuestion']);
        $reponseResult = $stmt->execute();
        $countRow = $reponseResult->fetchArray();
    ?>
    <body>
        <form  style="border:1px solid #ccc" method="GET" action="Traitement/AjoutQuestion.php">
            <div class="container">


            <h1>Ajouter une question(QCM)</h1>
            <hr>            

            <label><b>Numéro du Qcm : </b></label>
            <input type="text" placeholder="Ecrire le numéro du qcm..." id="numQcm" required>
            
            <label><b>Nom du Qcm : (si nouveau ou si vous souhaitez modifier) </b></label>
            <input type="text" placeholder="Ecrire le nom du qcm..." id="nom" >
            
            <label><b>Introduction du Qcm : (si nouveau ou si vous souhaitez modifier)</b></label>
            <input type="text" placeholder="Ecrire la phrase d'introduction du qcm..." id="intro" required>
            
            <label class="bareme-question"><b>Barème de la question : (Pour les questionnaires de type QCM) </b></label>
            <input class="bareme-question" type="text" placeholder="Ecrire le barème de la question..." id="bareme-question" >
            
            <label><b>Consigne : </b></label>
            <input type="text" placeholder="Ecrire l'intitulé de la question..." id="consigne" required>

            <b><label for="type">Type de questionnaire ?</label></b><br/><br/><div></div>
            <select name="type" id="type" class="type">
                <option value="vraifaux" label="QCM"></option>
                <option value="test" label="enquête"></option>
            </select>
            <br/><br/><div></div>
            <b><label for="nb_reponse">Nombre de choix de réponses ?</label></b><br/><br/><div></div>
            <select name="nb_reponse" id="nbReponses" class="1to100">
            <?php for($i=1;$i<100;$i++){
             ?><option value="<?php echo $i;?>"><?php echo $i;?></option>
        <?php } ?>
           
            </select>
                          <br/><br/><br/>
            
            <div id="ireponses"></div>
            
           
            <label id="labelBrep"><b>Bonne(s) Réponse(s) ? </b><br/>
                <div id="breponses"> </div>
                
            </label><br/>
            
            <div >
                <input type="button" value="Ajouter"  onclick="SubmitAjoutQCM()" />
            </div>
          </div>
        </form>
        <div id="erreur">
            
        </div>
        
        <form method="get" id="frm-qcm">
            <input id="inp-qcm" class="bouton" type="submit" name="action" value="QCM" > 
        </form>
        
       <input type="file" id="fileInput"> <br/>
       <label>QCM de type enquête : <strong>Numéro du Qcm**Nom du Qcm**test**Consigne**Choix1---points+++choix2---points+++(etc...)</strong> </label><br/>
        <label>QCM de type Vraix ou faux sous forme : <strong>Numéro du Qcm**Nom du Qcm**vraifaux**Consigne**Choix1+++Choix2+++(etc...)**numéro de bonne réponse 1,,numéro de bonne réponse 2(etc...)**bareme</strong> </label>
        <p> Tous les noms de table doivent être en majuscule</p>
    </body>    
       
    <script>
        
        $(document).ready(function() {
           $('#nbReponses').change(function() {
               $('#breponses').empty();
               $('#ireponses').empty();
                for(i=1 ; i<=$(this).val() ;i++){
                    if($('#type').val() !== 'test'){
                        $('#labelBrep').show();
                        $('#breponses').append('<input type="checkbox" id="rep'+i+'" name="reponse" value="'+i+'">'+i+'<br/>'); 
                        $('#ireponses').append('<label><b>Réponse '+i+' : </b></label>');
                        $('#ireponses').append('<input type="text" id="choix'+i+'" required>');
                    }
                    else {
                        $('#labelBrep').hide();
                        $('#ireponses').append('<label><b>Réponse '+i+' : </b></label>');
                        $('#ireponses').append('<input type="text" id="choix'+i+'" required>');
                        $('#ireponses').append('<input type="text" id="points'+i+'" name="bareme" placeholder="Ecrire le nombre de points pour cette réponse"><br/>');
                    }
                }
                
            });
            
            $('#type').change(function() {
                $('#breponses').empty();
                $('#ireponses').empty();
                if($(this).val() == 'test'){
                    $('#labelBrep').hide(); 
                    $('.bareme-question').hide();   
                    for(i=1 ; i<=$('#nbReponses').val() ;i++){
                        $('#ireponses').append('<label><b>Réponse '+i+' : </b></label>');
                        $('#ireponses').append('<input type="text" id="choix'+i+'" required>');
                        $('#ireponses').append('<input type="text" id="points'+i+'" name="bareme" placeholder="Ecrire le nombre de points pour cette réponse"><br/>');
                    }
                }
                else {
                    $('.bareme-question').show(); 
                    $( "input[name*='bareme']" ).remove();
                    $('#labelBrep').show();
                    for(i=1 ; i<=$('#nbReponses').val() ;i++){
                        $('#breponses').append('<input type="checkbox" id="rep'+i+'" name="reponse" value="'+i+'">'+i+'<br/>');
                        $('#ireponses').append('<label><b>Réponse '+i+' : </b></label>');
                        $('#ireponses').append('<input type="text" id="choix'+i+'" required>');                      
                    }
                }
            });
        });
        function SubmitAjoutQCM(mot=0) {
            var reponse = [];
            var choix = [];
            if(mot==0){
                var numQcm = $('#numQcm').val();
                var nomQcm = "existe";
                var type = $('#type').val();
                if($('#nom').val()){
                    nomQcm = $('#nom').val();
                }
               
                var nbRep = $('#nbReponses').val();

                for( var i = 1 ; i<=nbRep ; i++){
                    if ( $('#rep'+i).is( ":checked" ))
                        reponse.push($('#rep'+i).val());
                    if(type !== 'test')
                        choix.push($('#choix'+i).val());
                    else{
                        choix.push($('#choix'+i).val()+'---'+$('#points'+i).val())
                    }
                }

                var consigne = $('#consigne').val() ; 
                var intro = 'nomodif';
                if($('#intro').val()){
                     intro = $('#intro').val() ;
                }
                
                var baremequestion='test';
                if($('#bareme-question')){
                    baremequestion = $('#bareme-question').val();
                }
                
            }
            else {
                var numQcm = mot[0];
                var nomQcm = mot[1];
                var type = mot[2];
                var consigne = mot[3];
                var leschoix = mot[4].split('+++');
                
                for(var line = 0; line < leschoix.length; line++){
                    choix.push(leschoix[line]);
                }
                if(type == 'vraifaux'){
                    var brep = mot[5].split(',,');
                    for(var line = 0; line < brep.length; line++){
                        alert(consigne+'='+brep[line]);
                        reponse.push(brep[line]);
                    }
                    
                    var baremequestion = mot[6];
                }
            }             

            $.get("Traitement/AjoutQCM.php", {baremequestion : baremequestion, intro : intro,type : type , numQcm : numQcm , nomQcm : nomQcm,consigne: consigne, reponse : reponse, choix : choix},
                function(data) {
                     $('#erreur').html(data);
                    if (data.includes("AJOUT") == true){ 
                        /*var result = confirm("La question a été ajoutée. Afficher les QCM ?");
                        if(result)  $('#inp-qcm').trigger("click"); */ 
                    }
                     //$('#demoForm')[0].reset();
                  });
             }
             
        var fileInput = document.getElementById('fileInput');
        var fileDisplayArea = document.getElementById('fileDisplayArea');
        fileInput.addEventListener('change', function(e) {
            var file = fileInput.files[0];
            var textType = /text.*/;

            if (file.type.match(textType)) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                    var content = reader.result;
                    //Here the content has been read successfuly
                    //alert(content);
                    
                    var lignes = content.split('\n');
                    for(var line = 0; line < lignes.length; line++){
                        var mot = lignes[line].split('**');
                        //alert("fg");
                        if (mot != "")
                            SubmitAjoutQCM(mot);
                        //mots.push(mot);        
                        
                        //console.log(mot[0]);console.log(mot[1]);console.log(mot[2]);console.log(mot[3]);
                    }
                    //$('#inp-demo').trigger("click");
                }

                reader.readAsText(file);	
            } else {
                fileDisplayArea.innerText = "fichier non supporté"
            }
        });
              
    </script>
</html>
