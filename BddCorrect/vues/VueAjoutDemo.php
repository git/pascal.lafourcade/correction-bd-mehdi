 
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script src="css/jquery-3.5.1.js"></script>
        
        <link rel="stylesheet" href="css/VueAdmin.css">
    </head>

    <body>
        <form  style="border:1px solid #ccc" method="GET" action="Traitement/AjoutQuestion.php">
            <div class="container">


            <h1>Ajouter une Démonstration</h1>
            <hr>

            <label><b>Chapitre n° : </b></label>
            <input type="text" placeholder="Numéro du chapitre" id="chapitre" name='chapitre' required>

            <label><b>Description : </b></label>
            <input type="text" placeholder="Ecrire l'intitulé de la démonstration..." id="description" required>

            <label><b>Réponse : </b></label>
            <input type="text" placeholder="SELECT ..." id="reponse" required>
            
            
            <label><b>Base de données conçernée </b><br/>
                <input type="radio"  name="bdd" value="NBA" style="margin-bottom:15px" checked> NBA
            </label>

            <div>
                <input type="button" value="Ajouter"  onclick="SubmitAjoutDemo()" />
            </div>
          </div>
        </form>
        <div id="erreur">
            
        </div>
        
        <form method="get" id="frm-demo">
            <input id="inp-demo" class="bouton" type="submit" name="action" value="Afficher les demonstrations" > 
        </form><br/>
        
        <input type="file" id="fileInput"><br/>
        <div>Format : <strong>NBA**numéro de chapitre**description**requete</strong></div>

    </body>    
       
    <script>
        
        function SubmitAjoutDemo(mot=0) {
            
                     //err = jQuery('<p class')
                    if(mot == 0){
                        var chapitre = $('#chapitre').val() ; 
                        var description = $('#description').val() ; 
                        var reponse = $('#reponse').val() ; 
                        var bdd = $("input[name='bdd']:checked").val();               
                    }
                    else {
                        var bdd =mot[0];
                        var chapitre =  mot[1];
                        var description = mot[2];
                        var reponse =  mot[3];                       
                    }
                   $.get("Traitement/AjoutDemonstration.php", { chapitre: chapitre,description: description,reponse: reponse,bdd: bdd},
                    function(data) {
                        if(mot != 0){
                            
                            $('#erreur').append('<div class="err">'+data+'</div>');                             
                            //$('.err').html(data);
                            /*var result = confirm("La demonstration a été ajoutée. Afficher les Demonstrations ?");
                            if(result)  $('#inp-demo').trigger("click");  */
                            
                        }
                        else{
                            $('#erreur').empty();
                            setTimeout( function(){ $('#erreur').html(data);  }  , 400 );                        
                        }
                         //$('#demoForm')[0].reset();
                      });
        }
        
        
        var fileInput = document.getElementById('fileInput');
        var fileDisplayArea = document.getElementById('fileDisplayArea');
        fileInput.addEventListener('change', function(e) {
            var file = fileInput.files[0];
            var textType = /text.*/;

            if (file.type.match(textType)) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                    var content = reader.result;
                    //Here the content has been read successfuly
                    //alert(content);
                    
                    var lignes = content.split('\n');
                    for(var line = 0; line < lignes.length; line++){
                        var mot = lignes[line].split('**');
                        if (mot != "")
                            SubmitAjoutDemo(mot);
                        //mots.push(mot);        
                        
                        //console.log(mot[0]);console.log(mot[1]);console.log(mot[2]);console.log(mot[3]);
                    }
                    //$('#inp-demo').trigger("click");
                }

                reader.readAsText(file);	
            } else {
                fileDisplayArea.innerText = "fichier non supporté"
            }
        });



    </script>
</html>
