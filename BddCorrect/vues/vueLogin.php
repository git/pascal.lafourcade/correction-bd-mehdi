<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>f</title>
         <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" type="style/css" href="css/main.css"/>
         <style>
         body {
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #ADABAB;
         }
         
         .form-signin {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
            color: #017572;
         }
         
         .form-signin .form-signin-heading,
         .form-signin .checkbox {
            margin-bottom: 10px;
         }
         
         .form-signin .checkbox {
            font-weight: normal;
         }
         
         .form-signin .form-control {
            position: relative;
            height: auto;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding: 10px;
            font-size: 16px;
         }
         
         .form-signin .form-control:focus {
            z-index: 2;
         }
         
         .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
            border-color:#017572;
         }
         
         .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            border-color:#017572;
         }
         
         h2{
            text-align: center;
            color: #017572;
         }
      </style>   
    </head>
    <body>
        <h2>Connexion</h2> 
        <div class = "container form-signin">
        </div> 
        <div class = "container">
            <form class = "form-signin" role = "form" method = "post">
               <h4 class = "form-signin-heading"></h4>
               <input type = "text" class = "form-control" 
                  name = "identifiant" placeholder = "test" 
                  required autofocus></br>
               <input type = "password" class = "form-control"
                  name = "motdepasse" placeholder = "mdptest" required>
               <input type="submit" name="action" value="login"/>
            </form>
            

        </div> 
    </body>
</html>