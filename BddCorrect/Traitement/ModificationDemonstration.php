

        <link rel="stylesheet" href="css/VueAdmin.css">


<?php require_once('../BDD/SqliteDb.php');
require_once('../BDD/OracleDb.php');
$db = new SqliteDb('o');


$stmt = $db->prepare('SELECT * FROM Demonstration WHERE numDemo= ? AND chapitre = ?');
$stmt->bindParam(1, $_GET['numDemo']);
$stmt->bindParam(2, $_GET['numChapitre']);
$resultQuestion = $stmt->execute();
$questionRow = $resultQuestion->fetchArray();




?>
<form  style="border:1px solid #ccc" method="GET" >
  <div class="container">
    <h1>Modifier une démonstration</h1>
    <hr>

    <label><b>Chapitre n° : </b></label>
    <input type="text" placeholder="Numéro du TP" id="chapitre" value="<?php echo $_GET['numChapitre']; ?>" required disabled>

    <label><b>Numéro de démonstration : </b></label>
    <input type="text" placeholder="Saisir le numéro de question..." id="numDemo" value="<?php echo $_GET['numDemo']; ?>" required disabled>

    <label><b>Description : </b></label>
    <input type="text" placeholder="Ecrire la consigne..." id="description" value="<?php echo $questionRow['description']; ?>" required>

    <label><b>Réponse : </b></label>
    <input type="text" placeholder="SELECT ..." id="reponse" value="<?php echo $questionRow['reponse']; ?>" required>

    <label><b>Base de données conçernée </b><br/>
        <input type="radio"  name="bdd" value="NBA" style="margin-bottom:15px" checked> NBA
    </label>

    <input type="hidden" id="modif" value="modif" >
    <div class="clearfix">
        <input type="button" value="Modifier"  onclick="SubmitModifierDemonstration()" />
    </div>
  </div>
</form>
         <div id="result">
            
        </div>
        
        <form method="get" id="frm-demo">
            <input id="inp-demo" class="bouton" type="submit" name="action" value="Afficher les demonstrations" > 
        </form>
         <script>

        function SubmitModifierDemonstration() {
                    //document.write($('#demo'+numQuestion).val());
                    var chapitre = $('#chapitre').val() ; 
                    var numDemo = $('#numDemo').val() ; 
                    var description = $('#description').val() ; 
                    var reponse = $('#reponse').val() ; 
                    var bdd = $("input[name='bdd']:checked").val();   
                   var modif = 1;
                   $.get("Traitement/AjoutDemonstration.php", { modif: modif, chapitre: chapitre,numDemo: numDemo,description: description,reponse: reponse,bdd: bdd},
                    function(data) {          
                         $('#result').html(data);
                         //$('#demoForm')[0].reset();
                         
                      });
        }
        
        function AfficherDemonstrations(){
            location.reload();
        }



    </script>

