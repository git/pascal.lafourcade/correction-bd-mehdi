<?php

require_once('../BDD/SqliteDb.php');
require_once('../controleur/Controleur.php');


$db = new SqliteDb('o');

if (empty($_GET['tp']) || empty($_GET['bdd']) || empty($_GET['consigne']) || empty($_GET['reponse']) || empty($_GET['points']) || empty($_GET['aleatoire']) || empty($_GET['type'])) {
    echo '<strong>ERREUR1 : Veuillez remplir tous les champs </strong>';
}   
else if(!is_numeric($_GET['tp']))
    echo '<strong>ERREUR : une valeur non numérique à été détectée dans le champ \'TP n° :\' </strong>';
else if(!is_numeric($_GET['points']))
    echo '<strong>ERREUR : une valeur non numérique à été détectée dans le champ \'Barème:\' </strong>';
else {  
    
    $reponseQuery = $db->prepare('SELECT count(*) FROM type WHERE numTp=?');
    $reponseQuery->bindParam(1, $_GET['tp']);
    $reponseResult = $reponseQuery->execute();
    $reponseRow = $reponseResult->fetchArray();
    $numQuestion = $reponseRow['count(*)'] + 1;
    
    if(isset($_GET['modif'])){
        $numQuestion = $_GET['num'];
        if ($_GET["type"] == 'fonction')
            $db->supprimerFonction ($numQuestion, $_GET['tp']);
        else if ($_GET["type"] == 'requete')
            $db->supprimerRequete($numQuestion, $_GET['tp']);
    }
      
    if ($_GET["type"] == 'requete'){

        $isAdd = $db->ajouterRequete($_GET['bdd'],$_GET['tp'],$numQuestion,$_GET['consigne'], $_GET['reponse'],$_GET['points']);
        if(!$isAdd){
                echo '<strong>ERREUR : la combinaison (numéro de tp, numéro de question) existe déjà dans la bdd</strong>';
            }
        else{
            $trouve=0;
            $file = fopen('..\imports\tp\tp_historiqueAuto.txt', "a");
            $ligne = 'requete**'.$_GET['bdd'].'**'.$_GET['tp'].'**'.$_GET['consigne'].'**'.$_GET['reponse'].'**'.$_GET['points'].'**'.'n';
            $handle = @fopen('..\imports\tp\tp_historiqueAuto.txt', "r");
            if ($handle)
            {
                while (!feof($handle))
                {
                    $buffer = fgets($handle);
                    if(strpos($buffer, $ligne) !== FALSE)
                        $trouve = 1;
                }
                fclose($handle);
            }
            if($trouve==0) fwrite($file, $ligne."\n");
            $trouve = 0;
            fclose($file);
            echo '<strong>AJOUT Requete avec succès</strong>';
        }

    }
    else if ($_GET["type"] == 'fonction'){
        if(empty($_GET['fonction']))
            echo 'ERREUR : Veuillez remplir tous les champs  ';
        else {
            if(!isset($_GET['modif'])){
                $reponse = $_GET['reponse'];
                $re = '/^\s*SELECT\s*([a-z0-9]*)/i';
                preg_match($re, $reponse, $matches, PREG_OFFSET_CAPTURE);
                $string = 'f'.$matches[1][0];
                $freponse = str_replace($matches[1][0],$string, $reponse);

                $fonction = $_GET['fonction'];
                $re = '/^\s*CREATE OR REPLACE FUNCTION\s*([a-z0-9]*)/i';
                preg_match($re, $fonction, $matches2, PREG_OFFSET_CAPTURE);

                $string2 = 'f'.$matches2[1][0];
                $ffonction = str_replace($matches2[1][0],$string2, $fonction);                                                  
            }
            else {
                $freponse = $_GET['reponse'];
                $ffonction =  $_GET['fonction'];
            }
            
            $isAdd = $db->ajouterFonction($_GET['bdd'],$_GET['tp'],$numQuestion,$_GET['consigne'], $freponse,$ffonction,$reponse,$_GET['points']); 
            if(!$isAdd){
                echo '<strong>ERREUR : la combinaison (numéro de tp, numéro de question) existe déjà dans la bdd</strong>';
            }
            else{
                $trouve=0;
                $file = fopen('..\imports\tp\tp_historiqueAuto.txt', "a");
                $ligne = 'fonction**'.$_GET['bdd'].'**'.$_GET['tp'].'**'.$_GET['consigne'].'**'.$freponse.'**'.$_GET['points'].'**'.'n**'.$ffonction;
                $handle = @fopen('..\imports\tp\tp_historiqueAuto.txt', "r");
                if ($handle)
                {
                    while (!feof($handle))
                    {
                        $buffer = fgets($handle);
                        if(strpos($buffer, $ligne) !== FALSE)
                            $trouve = 1;
                    }
                    fclose($handle);
                }
                if($trouve==0) fwrite($file, $ligne."\n");
                $trouve = 0;
                fclose($file);
                echo '<strong>AJOUT Fonction avec succès</strong>';
            }
        }        
    }
    
    
} 
//require_once('index.php');
//header("Location: ../index.php");

