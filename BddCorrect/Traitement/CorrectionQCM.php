<?php

session_start();
require_once('../BDD/SqliteDb.php');
require_once('../BDD/OracleDb.php');
$db = new SqliteDb('o');

$oracleDb = new OracleDb();
$conn = $oracleDb->getConn();
$numReponseRow = array();


if(!isset($_GET['type'])){
    $query = $db->prepare('SELECT numReponse FROM QcmCorrection WHERE numQuestion=? AND numQcm= ?');
    $query->bindParam(1, $_GET['numQuestion']);
    $query->bindParam(2, $_GET['numQcm']);
    $result = $query->execute();

    while($r = $result->fetchArray() ){
        $numReponseRow[] = $r['numReponse'];
    }

    // Sort the array elements 
    sort($numReponseRow); 
    sort( $_GET['rep']); 

    // Check for equality 
    if ($numReponseRow ==  $_GET['rep']) 
        echo "<b>Bonne réponse ! </b>\n"; 
    else
        echo "<b>Mauvaise réponse.</b>\n"; 
}


else{
    $query = $db->prepare('SELECT count(*) FROM QcmQuestion WHERE  numQcm= ? ');
    $query->bindParam(1, $_GET['numQcm']);
    $result = $query->execute();
    $nbQuestionRow = $result->fetchArray();
    
    $total = 0;
    for($i = 1 ; $i<=$nbQuestionRow['count(*)'] ; $i++ ){
        $query = $db->prepare('SELECT max(points) FROM QcmReponse WHERE  numQcm= ? AND numQuestion=?');
        $query->bindParam(1, $_GET['numQcm']);
        $query->bindParam(2, $i);
        $result = $query->execute();
        $totalRow = $result->fetchArray();
        
        $total += $totalRow['max(points)'];
    }
    
    $query = $db->prepare('SELECT * FROM QcmReponse WHERE  numQcm= ? ');
    $query->bindParam(1, $_GET['numQcm']);
    $result = $query->execute();
    $totalRow = $result->fetchArray();

    
    $points = 0;
    for ($i = 1 ; $i < sizeof($_GET['rep']) ; $i++){
        $query = $db->prepare('SELECT * FROM QcmReponse WHERE numQuestion=? AND numQcm= ? AND numReponse= ?');
        $query->bindParam(1, $i);
        $query->bindParam(2, $_GET['numQcm']);
        $query->bindParam(3, $_GET['rep'][$i][0]);
        $result = $query->execute();
        $pointRow = $result->fetchArray();
        $points +=$pointRow['points'];
    }    
    echo '<b>Résultat : '.$points.' sur '.$total.'</b>';
}



