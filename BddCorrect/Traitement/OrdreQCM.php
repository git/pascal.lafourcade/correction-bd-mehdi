<?php

require_once('../BDD/SqliteDb.php');
$db = new SqliteDb('o');


$numQuestion = $_GET['numQuestion'];

$numQuestionPrec = $numQuestion-1;
$test = 999;

if(isset($_GET['des'])){
    $numQuestion += 1;
    $numQuestionPrec = $numQuestion-1;
}

if($numQuestion!=1){
          //QCM QUestion
    $sqliteQuery = $db->prepare('UPDATE QcmQuestion SET numQuestion=? WHERE numQuestion=? AND numQcm=?');   
    $sqliteQuery->bindParam(1, $test);
    $sqliteQuery->bindParam(2, $numQuestion);
    $sqliteQuery->bindParam(3, $_GET['numQcm']);
    $result = $sqliteQuery->execute();
      
    $sqliteQuery = $db->prepare('UPDATE QcmQuestion SET numQuestion=? WHERE numQuestion=? AND numQcm=?');
    $sqliteQuery->bindParam(1, $numQuestion);
    $sqliteQuery->bindParam(2, $numQuestionPrec);
    $sqliteQuery->bindParam(3, $_GET['numQcm']);
    $resulti = $sqliteQuery->execute();

    $sqliteQuery = $db->prepare('UPDATE QcmQuestion SET numQuestion=? WHERE numQuestion=? AND numQcm=?');
    $sqliteQuery->bindParam(1, $numQuestionPrec);
    $sqliteQuery->bindParam(2, $test);
    $sqliteQuery->bindParam(3, $_GET['numQcm']);
    $resultp = $sqliteQuery->execute();
    
    //même chose pour qcmreponse
    $sqliteQuery = $db->prepare('UPDATE QcmReponse SET numQuestion=? WHERE numQuestion=? AND numQcm=?');   
    $sqliteQuery->bindParam(1, $test);
    $sqliteQuery->bindParam(2, $numQuestion);
    $sqliteQuery->bindParam(3, $_GET['numQcm']);
    $result = $sqliteQuery->execute();
    
    $sqliteQuery = $db->prepare('UPDATE QcmReponse SET numQuestion=? WHERE numQuestion=? AND numQcm=?');
    $sqliteQuery->bindParam(1, $numQuestion);
    $sqliteQuery->bindParam(2, $numQuestionPrec);
    $sqliteQuery->bindParam(3, $_GET['numQcm']);
    $resulti = $sqliteQuery->execute();

    $sqliteQuery = $db->prepare('UPDATE QcmReponse SET numQuestion=? WHERE numQuestion=? AND numQcm=?');
    $sqliteQuery->bindParam(1, $numQuestionPrec);
    $sqliteQuery->bindParam(2, $test);
    $sqliteQuery->bindParam(3, $_GET['numQcm']);
    $resultp = $sqliteQuery->execute();
    
    
    //même chose pour QCMcorrection
    $sqliteQuery = $db->prepare('UPDATE QcmCorrection SET numQuestion=? WHERE numQuestion=? AND numQcm=?');   
    $sqliteQuery->bindParam(1, $test);
    $sqliteQuery->bindParam(2, $numQuestion);
    $sqliteQuery->bindParam(3, $_GET['numQcm']);
    $result = $sqliteQuery->execute();
    
    $sqliteQuery = $db->prepare('UPDATE QcmCorrection SET numQuestion=? WHERE numQuestion=? AND numQcm=?');
    $sqliteQuery->bindParam(1, $numQuestion);
    $sqliteQuery->bindParam(2, $numQuestionPrec);
    $sqliteQuery->bindParam(3, $_GET['numQcm']);
    $resulti = $sqliteQuery->execute();

    $sqliteQuery = $db->prepare('UPDATE QcmCorrection SET numQuestion=? WHERE numQuestion=? AND numQcm=?');
    $sqliteQuery->bindParam(1, $numQuestionPrec);
    $sqliteQuery->bindParam(2, $test);
    $sqliteQuery->bindParam(3, $_GET['numQcm']);
    $resultp = $sqliteQuery->execute();
    
}
 
//header("Location: index.php");


