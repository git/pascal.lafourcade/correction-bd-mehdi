<?php

require_once('../BDD/SqliteDb.php');
require_once('../controleur/Controleur.php');
$db = new SqliteDb('o');

$query = $db->prepare('SELECT count(*) FROM Qcm WHERE numQcm= ?');
$query->bindParam(1, $_GET['numQcm']);
$result = $query->execute();
$countRow = $result->fetchArray();


if($_GET['nomQcm'] == 'existe' && $countRow['count(*)'] == 0){
    exit("Le numéro de qcm n'existe pas : veuillez remplir le champ 'Nom du Qcm' ou utiliser un numéro de qcm existant");
}
else if($_GET['nomQcm'] !== 'existe' && $countRow['count(*)'] !== 0){
    $query = $db->prepare('UPDATE Qcm SET nom=? WHERE numQcm= ?');
    $query->bindParam(1, $_GET['nomQcm']);
    $query->bindParam(2, $_GET['numQcm']);
    $result = $query->execute();
    
}
else if($countRow['count(*)'] == 0){
    $query = $db->prepare('INSERT INTO Qcm VALUES(?,?,?,?)');
    $query->bindParam(1, $_GET['numQcm']);
    $query->bindParam(2, $_GET['nomQcm']);
    $query->bindParam(3, $_GET['type']);
    $query->bindParam(4, $_GET['intro']);
    $result = $query->execute();
    
}

if($_GET['intro'] !== 'nomodif' && $countRow['count(*)'] !== 0){
    $query = $db->prepare('UPDATE Qcm SET introduction=? WHERE numQcm= ?');
    $query->bindParam(1, $_GET['intro']);
    $query->bindParam(2, $_GET['numQcm']);
    $result = $query->execute();
}


$reponseQuery = $db->prepare('SELECT count(*) FROM QcmQuestion WHERE numQcm= ?');
$reponseQuery->bindParam(1, $_GET['numQcm']);
$reponseResult = $reponseQuery->execute();
$reponseRow = $reponseResult->fetchArray();

$numQuestion = $reponseRow['count(*)'] + 1;

$reponses = array();
if(isset($_GET['modif'])){
    $numQuestion -=1;
    $db->supprimerQCM ($numQuestion,$_GET['type']);
}

if($_GET['type']=='test'){
  
    $isAdd = $db->ajouterQCMTest($_GET['numQcm'],$numQuestion,$_GET['consigne'],$_GET['choix']);
    if(!$isAdd){
            echo '<strong>ERREUR : le numéro de question '.$numQuestion.' existe déjà dans le qcm '.$_GET['numQcm'].'</strong>';
    }
    else echo '<strong>AJOUT QCM avec succès</strong>';
}
else{
    $isAdd = $db->ajouterQCM($_GET['numQcm'],$numQuestion,$_GET['consigne'],$_GET['choix'],$_GET['reponse'],$_GET['baremequestion']);
    if(!$isAdd){
            echo '<strong>ERREUR : le numéro de question existe déjà dans la bdd</strong>';
    }
    else echo '<strong>AJOUT QCM avec succès</strong>';
}

