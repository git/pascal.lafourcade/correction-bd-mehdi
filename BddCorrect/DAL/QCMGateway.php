<?php //header('Content-type: text/plain');

 //require_once ('Question.php');
class QCMGateway {
    private $db;
   
    
    function __construct(SqliteDb $db) {
         $this->db=$db;
    }
    
    public function afficherQCM() {
        
        $query = 'SELECT * FROM Qcm ORDER BY numQcm';
        $query = $this->db->prepare($query);
        $resultqcm = $query->execute(); 
        while($qcm = $resultqcm->fetchArray()){
            $query = 'SELECT * FROM QcmQuestion WHERE numQcm = ? ORDER BY numQuestion ';
            $query = $this->db->prepare($query);
            $query->bindParam(1, $qcm['numQcm']);
            $result = $query->execute();       
            $tabQuestions = array();
            //$resultats = $this->db->query('SELECT found_rows()');     
            while($q = $result->fetchArray()){
                $reponseQuery = 'SELECT * FROM QcmReponse WHERE numQuestion=? AND numQcm = ? ORDER BY numReponse';
                $stmt = $this->db->prepare($reponseQuery);
                $stmt->bindParam(1, $q['numQuestion']);
                $stmt->bindParam(2, $qcm['numQcm']);
                $reponseResult = $stmt->execute();
                $tabReponses =array();
                while ($r = $reponseResult->fetchArray()){
                    $tabReponses[] = new ReponseQCM($r['numReponse'], $r['reponse'], $r['numQuestion']) ;               
                }
                
                $tabQuestions[] = new QuestionQCM($q['numQuestion'],$q['question'],$tabReponses,$q['bareme']);
            }
            $tabQcm[] = new QCM($qcm['numQcm'], $qcm['nom'], $qcm['type'], $tabQuestions, $qcm['introduction']) ;
            
        }
        //die(print_r($tabQcm, true ));
        //+die(print_r($tabQuestions, true ));
        return $tabQcm;
        //die('ook');
    }
    
}


