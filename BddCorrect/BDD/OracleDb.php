<?php



/**
 * Description of OracleDb
 *
 * @author Mehdi
 */
class OracleDb {
    
    private $conn;
    
    function __construct() {
        $this->conn = oci_connect('u_prems', '123456', 'localhost/orcl');
       //$this->conn = oci_connect('meelaichao', 'meelaichao', 'kirov:1521/kirov');
        //$this->createRandomTables();
    }
    
    function createRandomTables() {
        $this->createRandomStats();
        $this->createRandomJoueur();
        $this->createRandomEquipe();
        $this->createRandomGame();
    }
    
    function createRandomStats(){
        //Creation de la table
        $drop = oci_parse($this->conn, 'DROP TABLE randomstats');
        oci_execute($drop);
        $q = oci_parse($this->conn, 'CREATE TABLE randomstats AS SELECT * FROM stats WHERE 1=0');
        oci_execute($q);

        //Insertion Stats
        $tabStats = oci_parse($this->conn, 'SELECT * FROM stats');
        oci_execute($tabStats);

        $tabDate = array();
        $tabType = array();
        $tabEquipeJoueur = array();
        $tabLocation = array();
        $tabResultat = array();
        $tabPrenom = array();
        $tabStatus = array();
        $tabMinutes = array();
        $tabNaissance = array();
        $tabPoints = array();
        $tabPasses = array();
        $tabBallesPerdues = array();
        $tabInterceptions = array();
        $tabContres = array();
        $tabFautes = array();
        $tabTirsPris = array();
        $tabTirsMarques = array();
        $tabTirsPourcent = array();
        $tabTirs2Pris = array();
        $tabTirs2Marques = array();
        $tabTirs2Pourcent = array();
        $tabTirs3Pris = array();
        $tabTirs3Marques = array();
        $tabTirs3Pourcent = array();
        $tabLfPris = array();
        $tabLfMarques = array();
        $tabLfPourcent = array();
        $tabRebondDef = array();
        $tabRebondOff = array();
        $tabRebondTotal = array();
        $tabEquipeAdverse = array();

        while ($statsArray = oci_fetch_array($tabStats, OCI_NUM)) {
            $tabDate[] = $statsArray[0];
            $tabType[] = $statsArray[1];
            $tabEquipeJoueur[] = $statsArray[2];
            $tabLocation[] = $statsArray[3];
            $tabResultat[] = $statsArray[4];
            $tabPrenom[] = $statsArray[5];
            $tabStatus[] = $statsArray[6];
            $tabMinutes[] = $statsArray[7];
            $tabNaissance[] = $statsArray[8];
            $tabPoints[] = $statsArray[9];
            $tabPasses[] = $statsArray[10];
            $tabBallesPerdues[] = $statsArray[11];
            $tabInterceptions[] = $statsArray[12];
            $tabContres[] = $statsArray[13];
            $tabFautes[] = $statsArray[14];
            $tabTirsPris[] = $statsArray[15];
            $tabTirsMarques[] = $statsArray[16];
            $tabTirsPourcent[] = $statsArray[17];
            $tabTirs2Pris[] = $statsArray[18];
            $tabTirs2Marques[] = $statsArray[19];
            $tabTirs2Pourcent[] = $statsArray[20];
            $tabTirs3Pris[] = $statsArray[21];
            $tabTirs3Marques[] = $statsArray[22];
            $tabTirs3Pourcent[] = $statsArray[23];
            $tabLfPris[] = $statsArray[24];
            $tabLfMarques[] = $statsArray[25];
            $tabLfPourcent[] = $statsArray[26];
            $tabRebondDef[] = $statsArray[27];
            $tabRebondOff[] = $statsArray[28];
            $tabRebondTotal[] = $statsArray[29];
            $tabEquipeAdverse[] = $statsArray[30];
        }

        shuffle($tabDate);
        shuffle($tabType);
        shuffle($tabEquipeJoueur);
        shuffle($tabLocation);
        shuffle($tabResultat);
        shuffle($tabPrenom);
        shuffle($tabStatus);
        shuffle($tabMinutes);
        shuffle($tabNaissance);
        shuffle($tabPoints);
        shuffle($tabPasses);
        shuffle($tabBallesPerdues);
        shuffle($tabInterceptions);
        shuffle($tabContres);
        shuffle($tabFautes);
        shuffle($tabTirsPris);
        shuffle($tabTirsMarques);
        shuffle($tabTirsPourcent);
        shuffle($tabTirs2Pris);
        shuffle($tabTirs2Marques);
        shuffle($tabTirs2Pourcent);
        shuffle($tabTirs3Pris);
        shuffle($tabTirs3Marques);
        shuffle($tabTirs3Pourcent);
        shuffle($tabLfPris);
        shuffle($tabLfMarques);
        shuffle($tabLfPourcent);
        shuffle($tabRebondDef);
        shuffle($tabRebondOff);
        shuffle($tabRebondTotal);
        shuffle($tabEquipeAdverse);

        for ($i = 0; $i < sizeof($tabDate); $i++) {
            $insert = oci_parse($this->conn, 'INSERT INTO randomstats VALUES(\'' . $tabDate[$i] . ' \' , \''.$tabType[$i].'\' ,\'' . $tabEquipeJoueur[$i] . '\' , \'' . $tabLocation[$i] . '\' , \'' . $tabResultat[$i] . '\' , \'' . $tabPrenom[$i] . '\' , \'' . $tabStatus[$i] . '\' ,' . $tabMinutes[$i] . ' , \'' . $tabNaissance[$i] . ' \' ,'.$tabPoints[$i] . ' ,'.$tabPasses[$i] . ' ,'.$tabBallesPerdues[$i] . ' ,'.$tabInterceptions[$i] . ' ,'.$tabContres[$i] . ' ,'.$tabFautes[$i] . ' ,'.$tabTirsPris[$i] . ' ,'.$tabTirsMarques[$i] . ' ,'.$tabTirsPourcent[$i] . ' ,'.$tabTirs2Pris[$i] . ' ,'.$tabTirs2Marques[$i] . ' ,'.$tabTirs2Pourcent[$i] . ' ,'.$tabTirs3Pris[$i] . ' ,'.$tabTirs3Marques[$i] . ' ,'.$tabTirs3Pourcent[$i] . ' ,'.$tabLfPris[$i] . ' ,'.$tabLfMarques[$i] . ' ,'.$tabLfPourcent[$i] . ' ,'.$tabRebondDef[$i] . ' ,'.$tabRebondOff[$i] . ' ,'.$tabRebondTotal[$i] . ' , \'' . $tabEquipeAdverse[$i] . '\' ) ');
            oci_execute($insert);
            
        }
    }

    function createRandomJoueur(){
        //Creation de la table
        $drop = oci_parse($this->conn, 'DROP TABLE randomjoueur');
        oci_execute($drop);
        $q = oci_parse($this->conn, 'CREATE TABLE randomjoueur AS SELECT * FROM joueur WHERE 1=0');
        oci_execute($q);

        //Insertion Joueur
        $tabJoueur = oci_parse($this->conn, 'SELECT * FROM joueur');
        oci_execute($tabJoueur);
        
        $tabPrenomnomj = array();
        $tabAnneedebut = array();
        $tabAnneefin = array();
        $tabPoste = array();
        $tabTaille = array();
        $tabPoids = array();
        $tabDateNaissanceJ = array();
        
        while ($joueurArray = oci_fetch_array($tabJoueur, OCI_NUM)) {
            $tabPrenomnomj[] = $joueurArray[0];
            $tabAnneedebut[] = $joueurArray[1];
            $tabAnneefin[] = $joueurArray[2];
            $tabPoste[] = $joueurArray[3];
            $tabTaille[] = $joueurArray[4];
            $tabPoids[] = $joueurArray[5];
            $tabDateNaissanceJ[] = $joueurArray[6];      
        }
        
        shuffle($tabPrenomnomj);
        shuffle($tabAnneedebut);
        shuffle($tabAnneefin);
        shuffle($tabPoste);
        shuffle($tabTaille);
        shuffle($tabPoids);
        shuffle($tabDateNaissanceJ);
        
        for ($i = 0; $i < sizeof($tabPrenomnomj); $i++) {
            $insert = oci_parse($this->conn, 'INSERT INTO randomJoueur VALUES(\'' . $tabPrenomnomj[$i] . ' \' ,' . $tabAnneedebut[$i] . ' ,'.$tabAnneefin[$i] . ' , \'' . $tabPoste[$i] . '\', '.$tabTaille[$i] . ','.$tabPoids[$i] . ',\'' . $tabDateNaissanceJ[$i] . '\' ) ');
            oci_execute($insert);            
        }
               
    }
    
    function createRandomEquipe(){
        //Creation de la table
        $drop = oci_parse($this->conn, 'DROP TABLE randomequipe');
        oci_execute($drop);
        $q = oci_parse($this->conn, 'CREATE TABLE randomequipe AS SELECT * FROM equipe WHERE 1=0');
        oci_execute($q);
        
        //Insertion Joueur        
        $tabEquipe = oci_parse($this->conn, 'SELECT * FROM equipe');
        oci_execute($tabEquipe);
        
        $tabId = array();
        $tabVille = array();
        $tabNom = array();
        $tabConference = array();
        
        while ($equipeArray = oci_fetch_array($tabEquipe, OCI_NUM)) {
            $tabId[] = $equipeArray[0];
            $tabVille[] = $equipeArray[1];
            $tabNom[] = $equipeArray[2];
            $tabConference[] = $equipeArray[3];
        }
        
        shuffle($tabId);
        shuffle($tabVille);
        shuffle($tabNom);
        shuffle($tabConference);
        
        for ($i = 0; $i < sizeof($tabId); $i++) {
            $insert = oci_parse($this->conn, 'INSERT INTO randomequipe VALUES(\'' . $tabId[$i] . '\' ,\'' . $tabVille[$i] . '\', \'' . $tabNom[$i] . '\',  \'' . $tabConference[$i] . '\' ) ');
            oci_execute($insert);            
        }
    }
    
    function createRandomGame(){
         //Creation de la table
        $drop = oci_parse($this->conn, 'DROP TABLE randomgame');
        oci_execute($drop);
        $q = oci_parse($this->conn, 'CREATE TABLE randomgame AS SELECT * FROM game WHERE 1=0');
        oci_execute($q);
        
        //Insertion Joueur        
        $tabGame = oci_parse($this->conn, 'SELECT * FROM game');
        oci_execute($tabGame);
        
        $tabDate = array();
        $tabHoraire = array();
        $tabIdDomicile = array();
        $tabIdExterieur = array();
        $tabResultatDomicile = array();
        $tabResultatExterieur = array();
        $tabScoreDomicile = array();
        $tabScoreExterieur = array();
        
        while ($gameArray = oci_fetch_array($tabGame, OCI_NUM)) {
            $tabDate[] = $gameArray[0];
            $tabHoraire[] = $gameArray[1];
            $tabIdDomicile[] = $gameArray[2];
            $tabResultatDomicile[] = $gameArray[3];
            $tabScoreDomicile[] = $gameArray[4];
            $tabIdExterieur[] = $gameArray[5];
            $tabResultatExterieur[] = $gameArray[6];
            $tabScoreExterieur[] = $gameArray[7];
        }
        shuffle($tabDate);
        shuffle($tabHoraire);
        shuffle($tabIdDomicile);
        shuffle($tabResultatDomicile);
        shuffle($tabScoreDomicile);
        shuffle($tabIdExterieur);
        shuffle($tabResultatExterieur);
        shuffle($tabScoreExterieur);

        for ($i = 0; $i < sizeof($tabDate); $i++) {           
            $insert = oci_parse($this->conn, 'INSERT INTO randomgame VALUES(\'' . $tabDate[$i] . '\' ,' . $tabHoraire[$i] . ', \'' . $tabIdDomicile[$i] . '\', \''.$tabResultatDomicile[$i].'\', \''.$tabScoreDomicile[$i].'\', \''.$tabIdExterieur[$i].'\',  \''.$tabResultatExterieur[$i].'\',' .$tabScoreExterieur[$i].' ) ');
            oci_execute($insert);            
        }
    }
   
    public function getConn(){
        return $this->conn;
    }
}
