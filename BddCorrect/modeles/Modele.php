<?php


class Modele {
    
    function afficherQuestions(){
        $db = new SqliteDb();
        //$db->createTable();
        $qg = new QuestionsGateway($db);   

        $tabQuestions = $qg->afficherQuestions();
        return $tabQuestions;
    }
    
    function afficherDemonstrations(){
        $db = new SqliteDb();
        //$db->createTable();
        $qg = new QuestionsGateway($db);
        
        $tabDemo = $qg->afficherDemonstrations();
        return $tabDemo;
    }
    
    function afficherQCM(){
        $db = new SqliteDb();
        //$db->createTable();
        $qg = new QCMGateway($db);
        
        $tabQCM = $qg->afficherQCM();
        //die(print_r($tabQCM, true ));
        return $tabQCM;
    }
    
    function connection($id, $mdp){
        $ug = new UserGateway(new SqliteDb());
        $vraiMdp = $ug->getPassword($id);
        if(!password_verify($mdp, $vraiMdp)){
            return null;
        }
        return 1;
    }
    
    
}
