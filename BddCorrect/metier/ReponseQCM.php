<?php

class ReponseQCM {
    
    private $numReponse;
    private $reponse;
    private $numQuestion;
    
    function __construct($numReponse,$reponse,$numQuestion){
        $this->numReponse = $numReponse;
        $this->reponse = $reponse;
        $this->numQuestion = $numQuestion;
    }
    
    function getNumReponse() {
        return $this->numReponse;
    }

    function getReponse() {
        
        return $this->reponse;
    }

    function getNumQuestion() {
        return $this->numQuestion;
    }

 public function __toString(){    
    $output = ''.$this->getReponse();    
      
    return $output;
  }

    
}


