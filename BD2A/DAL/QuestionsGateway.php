<?php
 //require_once ('Question.php');


class QuestionsGateway {
    private $db;
   
    
    function __construct(SqliteDb $db) {
         $this->db=$db;
    }
    
    public function afficherQuestions() {
        $tabQuestions=array();
        $query = 'SELECT numTp FROM Correct  UNION SELECT numTp FROM FunctionCorrect';
        $query = $this->db->prepare($query);
        $result = $query->execute();
        //$nbTpRow = $result->fetchArray(SQLITE3_NUM);
        $i = 0;
        while ($tp = $result->fetchArray(SQLITE3_NUM)){
            $questionQuery = 'SELECT numTp,numquestion,question,reponse,points FROM Correct WHERE numTp = ?  UNION SELECT numTp,numquestion,question,reponse,points FROM FunctionCorrect WHERE numTp = ? ';
            $questionQuery = $this->db->prepare($questionQuery);
            $questionQuery->bindParam(1, $tp[0]);
            $questionQuery->bindParam(2, $tp[0]);
            $questionResult = $questionQuery->execute();
            while($q = $questionResult->fetchArray()){
                $tabQuestions[] = new QuestionNote($q['numTp'],$q['numquestion'],$q['question'],$q['reponse'],$q['points']);
            }
            $tabTp['tp'.$tp[0]] = $tabQuestions ;  
            unset($tabQuestions);
            $tabQuestions = array();            
        }
        
        //die("<pre>".print_r($tabTp)."</pre");
        return $tabTp;
    }
    
    public function afficherDemonstrations() {
        $tabQuestions=array();
        $query = 'SELECT chapitre FROM Demonstration';
        $query = $this->db->prepare($query);
        $result = $query->execute();
        //$nbTpRow = $result->fetchArray(SQLITE3_NUM);
        $i = 0;
        while ($chapitre = $result->fetchArray(SQLITE3_NUM)){
            $questionQuery = 'SELECT chapitre,numDemo,description,reponse FROM Demonstration WHERE chapitre = ? ORDER BY numDemo';
            $questionQuery = $this->db->prepare($questionQuery);
            $questionQuery->bindParam(1, $chapitre[0]);
            $questionResult = $questionQuery->execute();
            while($q = $questionResult->fetchArray()){
                $tabQuestions[] = new Question($q['chapitre'],$q['numDemo'],$q['description'],$q['reponse']);
            }
            $tabChapitre['chapitre'.$chapitre[0]] = $tabQuestions ;  
            unset($tabQuestions);
            $tabQuestions = array();            
        }
        
        //die("<pre>".print_r($tabTp)."</pre");
        return $tabChapitre;
    }
}
