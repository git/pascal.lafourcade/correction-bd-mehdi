<?php

require_once('../BDD/SqliteDb.php');
$db = new SqliteDb('o');

if ($_GET['type']=='demo'){
    $file = fopen('..'.DIRECTORY_SEPARATOR.'imports'.DIRECTORY_SEPARATOR.'demonstration'.DIRECTORY_SEPARATOR.'demo_exportes.txt', "w");
    $matches = array();
    $trouve = 0;
    $reponseQuery = $db->prepare('SELECT * FROM demonstration');
    $reponseResult = $reponseQuery->execute();
    while ($r = $reponseResult->fetchArray()){
        $ligne = $r['bddconcernee'].'**'.$r['chapitre'].'**'.$r['description'].'**'.$r['reponse'];
        $handle = @fopen('..'.DIRECTORY_SEPARATOR.'imports'.DIRECTORY_SEPARATOR.'demonstration'.DIRECTORY_SEPARATOR.'demo_exportes.txt', "r");
        if ($handle)
        {
            while (!feof($handle))
            {
                $buffer = fgets($handle);
                if(strpos($buffer, $ligne) !== FALSE)
                    $trouve = 1;
            }
            fclose($handle);
        }
        if($trouve==0) fwrite($file, $ligne."\n");
        $trouve = 0;
        //fwrite($file, $ligne."\n");
    }
    fclose($file);    
   // file_put_contents('..\imports\demonstration\demo_exportes.txt', rtrim(file_get_contents('..\imports\demonstration\demo_exportes.txt')));
}

else if ($_GET['type']=='tp'){
    $file = fopen('..'.DIRECTORY_SEPARATOR.'imports'.DIRECTORY_SEPARATOR.'tp'.DIRECTORY_SEPARATOR.'tp_exportes.txt', "w");
    $matches = array();
    $trouve = 0;
    $reponseQuery = $db->prepare('SELECT bddConcernee,numTp,numquestion,question,reponse,points FROM Correct UNION SELECT bddConcernee,numTp,numquestion,question,reponse,points FROM FunctionCorrect');
    $reponseResult = $reponseQuery->execute();
    while ($r = $reponseResult->fetchArray()){
        $questionQuery = 'SELECT type FROM type WHERE numQuestion = ? AND numTp=? ';
        $questionQuery = $db->prepare($questionQuery);
        $questionQuery->bindParam(1, $r['numquestion']);
        $questionQuery->bindParam(2, $r['numTp']);
        $questionResult = $questionQuery->execute();
        $typeRow = $questionResult->fetchArray();
        $reponse = $r['reponse'];
        if($typeRow['type'] == 'functionCorrect'){
            $questionQuery = 'SELECT fonctionCorrect FROM functionCorrect WHERE numQuestion = ? AND numTp=? ';
            $questionQuery = $db->prepare($questionQuery);
            $questionQuery->bindParam(1, $r['numquestion']);
            $questionQuery->bindParam(2, $r['numTp']);
            $questionResult = $questionQuery->execute();
            $fonctionRow = $questionResult->fetchArray();
            $type='fonction';
            

            $re = '/^\s*SELECT\s*([a-z0-9])/i';
            preg_match($re, $reponse, $matches, PREG_OFFSET_CAPTURE);
            $string = 'f'.$matches[1][0];
            $freponse = str_replace($matches[1][0],'', $reponse);

            $fonction = $fonctionRow['fonctionCorrect'];
            $re = '/^\s*CREATE OR REPLACE FUNCTION\s*([a-z0-9])/i';
            preg_match($re, $fonction, $matches2, PREG_OFFSET_CAPTURE);

            $string2 = 'f'.$matches2[1][0];
            $ffonction = str_replace($matches2[1][0],'', $fonction);
        }
        else{
            $freponse = $reponse;           
            $type='requete';
        }
        $ligne = $type.'**'.$r['bddConcernee'].'**'.$r['numTp'].'**'.$r['question'].'**'.$freponse.'**'.$r['points'].'**n';
        
        if($typeRow['type'] == 'functionCorrect'){
            $ligne = $ligne.'**'.$ffonction;
        }
        $handle = @fopen('..'.DIRECTORY_SEPARATOR.'imports'.DIRECTORY_SEPARATOR.'tp'.DIRECTORY_SEPARATOR.'tp_exportes.txt', "r");
        if ($handle)
        {
            while (!feof($handle))
            {
                $buffer = fgets($handle);
                if(strpos($buffer, $ligne) !== FALSE)
                    $trouve = 1;
            }
            fclose($handle);
        }
        if($trouve==0) fwrite($file, $ligne."\n");
        $trouve = 0;
        //fwrite($file, $ligne."\n");
    }
    fclose($file);    
   // file_put_contents('..\imports\demonstration\demo_exportes.txt', rtrim(file_get_contents('..\imports\demonstration\demo_exportes.txt')));
 


}

else if($_GET['type']=='qcm'){
   $file = fopen('..'.DIRECTORY_SEPARATOR.'imports'.DIRECTORY_SEPARATOR.'qcm'.DIRECTORY_SEPARATOR.'qcm_exportes.txt', "w");
   if(!$file) echo 'FILE';
    $matches = array();
    $trouve = 0;
    $reponseQuery = $db->prepare('SELECT * FROM Qcm ORDER By numQcm');
    $reponseResult = $reponseQuery->execute();
    while ($qcm = $reponseResult->fetchArray()){

        $ligne = $qcm['numQcm'].'**'.$qcm['nom'].'**'.$qcm['type'];
        $reponseQuery = $db->prepare('SELECT * FROM QcmQuestion WHERE numQcm=? ORDER By question');
        $reponseQuery->bindParam(1, $qcm['numQcm']);
        $questionResult = $reponseQuery->execute();
        
        while ($question = $questionResult->fetchArray()){
            $ligne = $qcm['numQcm'].'**'.$qcm['nom'].'**'.$qcm['type'];
            $ligne = $ligne.'**'.$question['question'];
            
            $reponseQuery = $db->prepare('SELECT * FROM QcmReponse WHERE numQcm=? AND numQuestion=? ORDER BY numReponse');
            $reponseQuery->bindParam(1, $qcm['numQcm']);
            $reponseQuery->bindParam(2, $question['numQuestion']);
            $reponsResult = $reponseQuery->execute();
            if($qcm['type'] == 'test'){
                $ligne = $ligne.'**';
                while ($reponseTest = $reponsResult->fetchArray()){
                    $ligne = $ligne.$reponseTest['reponse'].'---'.$reponseTest['points'].'+++';
                }
                $ligne = substr($ligne, 0, -3);
            }
            else{
                $ligne = $ligne.'**';
                while ($reponseV = $reponsResult->fetchArray()){
                    $ligne = $ligne.$reponseV['reponse'].'+++';
                }
                $ligne = substr($ligne, 0, -3);
                $ligne = $ligne.'**';
                $reponseQuery = $db->prepare('SELECT * FROM QcmCorrection WHERE numQcm=? AND numQuestion=? ORDER BY numQcm,numQuestion');
                $reponseQuery->bindParam(1, $qcm['numQcm']);
                $reponseQuery->bindParam(2, $question['numQuestion']);
                $reponsResult = $reponseQuery->execute();
                while ($reponse = $reponsResult->fetchArray()){
                    $ligne = $ligne.$reponse['numReponse'].',,';
                }
                $ligne = substr($ligne, 0, -2);
                
            }
            $ligne = $ligne.'**'.$qcm['introduction'];
            $trouve = 0;
            $handle = @fopen('..'.DIRECTORY_SEPARATOR.'imports'.DIRECTORY_SEPARATOR.'qcm'.DIRECTORY_SEPARATOR.'qcm_exportes.txt', "r");
            if ($handle)
            { 
                while (!feof($handle))
                {
                    $buffer = fgets($handle);
                    if(strpos($buffer, $ligne) !== FALSE)
                        $trouve = 1;
                }
                fclose($handle);
            }
            if($trouve==0){
                fwrite($file, $ligne."\n");
            } 
            
        }
    }
    fclose($file); 
}