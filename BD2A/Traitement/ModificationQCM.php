 
        <link rel="stylesheet" href="css/VueAdmin.css">


<?php require_once('../BDD/SqliteDb.php');
require_once('../BDD/OracleDb.php');
$db = new SqliteDb('o');
session_start();


$stmt = $db->prepare('SELECT * FROM Qcm WHERE numQcm= ?');
$stmt->bindParam(1, $_GET['numQcm']);
$resultQuestion = $stmt->execute();
$qcmRow = $resultQuestion->fetchArray();


$stmt = $db->prepare('SELECT * FROM QcmQuestion WHERE numQuestion= ? AND numQcm=?');
$stmt->bindParam(1, $_GET['numQuestion']);
$stmt->bindParam(2, $_GET['numQcm']);
$resultQuestion = $stmt->execute();
$questionRow = $resultQuestion->fetchArray();

$stmt = $db->prepare('SELECT reponse FROM QcmReponse WHERE numQuestion= ? AND numQcm=?');
$stmt->bindParam(1, $_GET['numQuestion']);
$stmt->bindParam(2, $_GET['numQcm']);
$resultQuestion = $stmt->execute();
while($r = $resultQuestion->fetchArray(SQLITE3_NUM)){
    $reponseRow[] = $r[0];
}

$stmt = $db->prepare('SELECT count(*) FROM QcmReponse WHERE numQuestion= ? AND numQcm=?');
$stmt->bindParam(1, $_GET['numQuestion']);
$stmt->bindParam(2, $_GET['numQcm']);
$resultQuestion = $stmt->execute();
$countRow = $resultQuestion->fetchArray();

while($r = $resultQuestion->fetchArray(SQLITE3_NUM)){
    $reponseRow[] = $r[0];
}

$stmt = $db->prepare('SELECT * FROM QcmCorrection WHERE numQuestion= ? AND numQcm=?');
$stmt->bindParam(1, $_GET['numQuestion']);
$stmt->bindParam(2, $_GET['numQcm']);
$resultQuestion = $stmt->execute();
$correctRow = $resultQuestion->fetchArray();

?>
        <form  style="border:1px solid #ccc" method="GET" action="Traitement/AjoutQuestion.php">
            <div class="container">


            <h1>Ajouter une question(QCM)</h1>
            <hr>            

            <label><b>Numéro du Qcm : </b></label>
            <input type="text" placeholder="Ecrire le numéro du qcm..." id="numQcm" value="<?php echo $_GET['numQcm']; ?>" disabled>
            
            <label><b>Nom du Qcm :  </b></label>
            <input type="text" placeholder="Ecrire le nom du qcm..." id="nom" value="<?php echo $qcmRow['nom']; ?>" >
            
            <label><b>Barème du Qcm : (Pour les questionnaires de type QCM- si nouveau ou si vous souhaitez modifier) </b></label>
            <input type="text" placeholder="Ecrire le barème du qcm..." id="baremeqcm" value="<?php echo $questionRow['bareme']; ?>" >
            
            <label><b>Introduction du Qcm : </b></label>
            <input type="text" placeholder="Ecrire la phrase d'introduction du qcm..." id="intro" value="<?php echo $qcmRow['introduction']; ?>" required>
            
            <label><b>Numéro de la question :  </b></label>
            <input type="text" placeholder="Ecrire le nom du qcm..." id="numQuestion" value="<?php echo $_GET['numQuestion'];?>" disabled>
            
            <label><b>Consigne : </b></label>
            <input type="text" placeholder="Ecrire l'intitulé de la question..." id="consigne" value="<?php echo $questionRow['question'];?>" required>

            <b><label for="type">Type de Qcm ?</label></b><br/><br/><div></div>
            <select name="type" id="type" class="type" disabled>
                <option value="vraifaux" label="Vrai ou Faux" <?php if($qcmRow['type'] == 'vraifaux') echo 'selected'; ?>></option>
                <option value="test" label="enquête" <?php if($qcmRow['type'] == 'test') echo 'selected'; ?>></option>
            </select>
            <br/><br/><div></div>
            
            <b><label for="nb_reponse">Nombre de choix de réponses ?</label></b><br/><br/><div></div>
            <select name="nb_reponse" id="nbReponses" class="1to100">
            <?php for($i=1;$i<100;$i++){
             ?><option value="<?php echo $i;?>" <?php if($countRow['count(*)'] == $i) echo 'selected'; ?>><?php echo $i;?></option>
        <?php } ?>
           
            </select>
            <br/><br/><br/>

            <div id="ireponses">
         <?php  if($qcmRow['type'] !== 'test'){ 
                    for($i=1 ; $i<=$countRow['count(*)'] ;$i++){ ?>
                        <label><b>Réponse <?php echo $i;?> : </b></label>
                        <?php
                            $stmt = $db->prepare('SELECT reponse FROM QcmReponse WHERE numQcm=? AND numQuestion=? AND numReponse=?');
                            $stmt->bindParam(1, $_GET['numQcm']);
                            $stmt->bindParam(2, $_GET['numQuestion']);
                            $stmt->bindParam(3, $i);
                            $resultQuestion = $stmt->execute();
                            $repRow = $resultQuestion->fetchArray();
                        
                         ?>
                        <input type="text" id="choix<?php echo $i;?>"  value="<?php echo $repRow['reponse'];?>" required>
           <?php    }
                }
                else{
                    for($i=1 ; $i<=$countRow['count(*)'] ;$i++){ 
                        $stmt = $db->prepare('SELECT reponse,points FROM QcmReponse WHERE numQcm=? AND numQuestion=? AND numReponse=?');
                        $stmt->bindParam(1, $_GET['numQcm']);
                        $stmt->bindParam(2, $_GET['numQuestion']);
                        $stmt->bindParam(3, $i);
                        $resultQuestion = $stmt->execute();
                        $repRow = $resultQuestion->fetchArray(); ?>
                        <label><b>Réponse <?php echo $i;?> : </b></label>
                        <input type="text" id="choix<?php echo $i;?>" value="<?php echo $repRow['reponse'];?>" required>
                        <input type="text" id="points<?php echo $i;?>" name="bareme" placeholder="Ecrire le nombre de points pour cette réponse" value="<?php echo $repRow['points'];?>"><br/>
        <?php       }
                }   ?>
            </div>
            
            
           
            <label id="labelBrep"><b>Bonne(s) Réponse(s) ? </b><br/>
                <div id="breponses"> 
        <?php   if($qcmRow['type'] !== 'test'){ 
                    for($i=1 ; $i<=$countRow['count(*)'] ;$i++){ 
                        $stmt = $db->prepare('SELECT count(*) FROM QcmCorrection WHERE numQuestion= ? AND numQcm=? AND numReponse=?');
                        $stmt->bindParam(1, $_GET['numQuestion']);
                        $stmt->bindParam(2, $_GET['numQcm']);
                        $stmt->bindParam(3, $i);
                        $resultQuestion = $stmt->execute();
                        $BrepRow = $resultQuestion->fetchArray();
                        if($BrepRow['count(*)']==0) $isChecked='';
                        else $isChecked='checked';
                        ?>
                    <input type="checkbox" id="rep<?php echo $i;?>" name="reponse" value="<?php echo $i;?>" <?php echo $isChecked;?>><?php echo $i;?><br/>                        
           <?php    }
                }?>
                </div>
            </label><br/>
            
            <div >
                <input type="button" value="Modifier"  onclick="SubmitModifierQCM()" />
            </div>
          </div>
        </form>
        <div id="erreur">
            
        </div>
        
        <form method="get">
            <input id="inp-qcm" class="bouton" type="submit" name="action" value="QCM" > 
        </form>
        
       
    </body>    
       
    <script>
        
        function SubmitModifierQCM() {
            var reponse = [];
            var choix = [];
                var numQcm = $('#numQcm').val();
                var type = $('#type').val();
                var nomQcm = $('#nom').val();
                              
                var nbRep = $('#nbReponses').val();

                for( var i = 1 ; i<=nbRep ; i++){
                    if ( $('#rep'+i).is( ":checked" ))
                        reponse.push($('#rep'+i).val());
                    if(type !== 'test')
                        choix.push($('#choix'+i).val());
                    else{
                        choix.push($('#choix'+i).val()+'---'+$('#points'+i).val())
                    }
                }
                var consigne = $('#consigne').val() ; 
                var modif = 1;
                 var baremequestion='test';
                if($('#bareme-question')){
                    baremequestion = $('#bareme-question').val();
                }
                
                var intro = $('#intro').val() ;
                var numQuestion = $('#numQuestion').val() ;
            $.get("Traitement/AjoutQCM.php", {numQuestion : numQuestion,baremequestion : baremequestion,intro : intro, type : type , numQcm : numQcm , nomQcm : nomQcm,consigne: consigne, reponse : reponse, choix : choix ,modif:modif},
                function(data) {
                     $('#erreur').html(data);
                    if (data.includes("AJOUT") == true){ 
                        /*var result = confirm("La question a été ajoutée. Afficher les QCM ?");
                        if(result)  $('#inp-qcm').trigger("click"); */ 
                    }
                     //$('#demoForm')[0].reset();
                  });
    }
              
    </script>
</html>
