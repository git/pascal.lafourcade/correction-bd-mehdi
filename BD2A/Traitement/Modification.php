

        <link rel="stylesheet" href="css/VueAdmin.css">


<?php require_once('../BDD/SqliteDb.php');
require_once('../BDD/OracleDb.php');
$db = new SqliteDb('o');




$typeQuery = $db->prepare('SELECT type FROM Type WHERE numQuestion = ? AND numTp= ?');
$typeQuery->bindParam(1, $_GET['numQuestion']);
$typeQuery->bindParam(2, $_GET['numTp']);
$resultType = $typeQuery->execute();
$typeRow = $resultType->fetchArray();

if($typeRow['type']== 'query')
    $stmt = $db->prepare('SELECT * FROM Correct WHERE numQuestion= ? AND numTp = ?');
else if($typeRow['type']== 'functionCorrect')
    $stmt = $db->prepare('SELECT question,reponse,fonctionCorrect,points FROM FunctionCorrect WHERE numQuestion= ? AND numTp = ?');

$stmt->bindParam(1, $_GET['numQuestion']);
$stmt->bindParam(2, $_GET['numTp']);
$resultQuestion = $stmt->execute();
$questionRow = $resultQuestion->fetchArray();




?>
<form  style="border:1px solid #ccc" method="GET" >
  <div class="container">
    <h1>Modifier une question</h1>
    <hr>

    <label><b>TP n° : </b></label>
    <input type="text" placeholder="Numéro du TP" id="tp" value="<?php echo $_GET['numTp']; ?>" required disabled>

    <label><b>Numéro de question : </b></label>
    <input type="text"  placeholder="Saisir le numéro de question..." id="num" value="<?php echo $_GET['numQuestion']; ?>" required disabled>

    <label><b>Barème : </b></label>
    <input type="text" placeholder="Barème..." id="points"  value="<?php echo $questionRow['points']; ?>" required>
    
    <label><b>Consigne : </b></label>
    <textarea placeholder="Ecrire la consigne..." id="consigne" required><?php echo $questionRow['question']; ?></textarea>

    <label><b>Réponse : </b></label>
    <textarea placeholder="SELECT ..." id="reponse" required><?php echo $questionRow['reponse']; ?></textarea>

     <input class="executerReponse" type="button" value="Exécuter" onclick="ExecuterReponse();" > <br/><br/>
    <div id="execute">
            
        </div><div><br/><br/><br/></div>

    <input type="hidden" id="modif" value="modif" >
    <div class="clearfix">
        <input type="button" value="Modifier"  onclick="SubmitModifierQuestion()" />
    </div>
  </div>
</form>
         <div id="erreur">
            
        </div> 
        
        <form method="get" id="frm-tp">
            <input id="inp-tp" class="bouton" type="submit" name="action" value="Afficher les questions" > 
        </form>
         <script>
        
        $(document).ready(function() {
            $('input[type="radio"]').click(function() {
                if($(this).attr('id') == 'btn-fonction') {
                     $('#show-me').show();           
                }

                else {
                     $('#show-me').hide();   
                }
            });
            if ($("#btn-fonction").prop("checked")) {
                 $('#show-me').show();  
            }
        });

        function ExecuterReponse() {
                    //document.write($('#demo'+numQuestion).val());
                    var tx = $('#reponse').val() ;
                    var txt =  tx.replace(/^\s+/g, "");

                    txt = txt.trim();

                    if(!(txt.toUpperCase().includes('CREATE') && txt.toUpperCase().includes('FUNCTION'))) {
                        if (txt.charAt(txt.length - 1) == ';') {
                          txt = txt.substr(0, txt.length - 1);
                        }
                    }
                    $.get("Traitement/AffichageDemo.php", { textbox: txt},
                    function(data) {
                          $('#execute').empty();
                            setTimeout( function(){ $('#execute').html(data);  }  , 400 );  
                         //$('#demoForm')[0].reset();
                      });
        } 
        function SubmitModifierQuestion() {
                    //document.write($('#demo'+numQuestion).val());


                    var tp = $('#tp').val() ; 
                    var num = $('#num').val() ; 
                    var points = $('#points').val() ; 
                    var consigne = $('#consigne').val() ; 
                    var reponse = $('#reponse').val() ; 
                    //var type = $("input[name='type']:checked").val();
                    var type = 'requete'; 
                    var aleatoire= $("input[name='aleatoire']:checked").val(); 
                    //var bdd = $("input[name='bdd']:checked").val();
                    var bdd = 'NBA';
                    var fonction = $('#fonction').val() ;    
                    var modif = $('#modif').val();


                    var tx = reponse;
                    
                    var txt =  tx.replace(/^\s+/g, "");

                    txt = txt.trim();

                    if(!(txt.toUpperCase().includes('CREATE') && txt.toUpperCase().includes('FUNCTION'))) {
                        if (txt.charAt(txt.length - 1) == ';') {
                          txt = txt.substr(0, txt.length - 1);
                        }
                    }
                    reponse = txt; 
                   $.get("Traitement/AjoutQuestion.php", { modif: modif, tp: tp,num: num,points: points,consigne: consigne,reponse: reponse,type: type,aleatoire: aleatoire,bdd: bdd, fonction : fonction},
                    function(data) {
                         $('#erreur').html(data);
                         //$('#demoForm')[0].reset();
                         
                      });
        }
        
        function AfficherQuestions(){
            location.reload();
        }



    </script>

