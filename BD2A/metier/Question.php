<?php

class Question {
    
    private $numQuestion;
    private $question;
    private $reponse;
    private $numTp;
    
    function __construct($numTp,$numQuestion,$question,$reponse){
        $this->numQuestion = $numQuestion;
        $this->question = $question;
        $this->reponse = $reponse;
        $this->numTp = $numTp ; 
    }
    
    // Getters
    function getNumQuestion() {
        return $this->numQuestion;
    }

    function getQuestion() {
        return $this->question;
    }

    function getReponse() {
        return $this->reponse;
    }

    function getNumTp() {
        return $this->numTp;
    }
    
    // Setters
    function setNumQuestion($numQuestion) {
        $this->numQuestion = $numQuestion;
    }

    function setQuestion($question) {
        $this->question = $question;
    }

    function setReponse($reponse) {
        $this->reponse = $reponse;
    }

    
}


