<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QCM
 *
 * @author Mehdi
 */
class QCM {
    private $num;
    private $nom;
    private $type;
    private $questions = array();
    private $introduction;
    
    function __construct($numQuestion,$nom,$type,$questions,$introduction){
        $this->num = $numQuestion;
        $this->nom = $nom;
        $this->type = $type;
        $this->questions = $questions;
        $this->introduction = $introduction;
    }
    
    // Getters
   
    function getNum() {
        return $this->num;
    }

    function getNom() {
        return $this->nom;
    }

    function getType() {
        return $this->type;
    }

    function getQuestions() {
        return $this->questions;
    }

    function getIntroduction() {
        return $this->introduction;
    }

    
    // Setters
    function setIntroduction($introduction) {
        $this->introduction = $introduction;
    }


    function setNum($num) {
        $this->num = $num;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setQuestions($questions) {
        $this->questions = $questions;
    }


}
