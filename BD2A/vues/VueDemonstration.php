<!DOCTYPE html>

<html>

    <head>
        <meta charset="UTF-8">
        <title></title>
        <script src="css/jquery-3.5.1.js"></script>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/VueDemonstration.css">
    </head>
    <?php 
      $tp_str = implode(",", array_keys($dVueDemonstrations));
      //die($tp_str);
   
    ?>
    <body id="vued">
         <input type="text" id='tp_keys' value="<?php echo $tp_str; ?>" hidden>
        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="mainNav">
               <div class="container">
                   <a class="navbar-brand js-scroll-trigger" href="#page-top" onclick="scrollToTop">Base De Données</a>                 
                 <div class="collapse navbar-collapse" id="navbarResponsive">
                   <ul class="navbar-nav ml-auto">
                       <?php                    foreach ($dVueDemonstrations as $tp => $question){   ?>
                     <li class="nav-item">
                         <a style = "text-transform:uppercase;" class="nav-link js-scroll-trigger" id="<?php echo $tp; ?>" onclick="changerSection('<?php echo $tp;?>','<?php echo $tp_str ;?>')" ><?php echo $tp; ?></a>
                     </li>                
                       <?php }?>
                   </ul>
                 </div>
               </div>
             </nav>
            <header class="bg-primary text-white">
              <div class="container text-center">
                <h1>Demonstrations</h1>
                <p class="lead"></p>
              </div>
            </header>
            <form method="get" >
                <input id="btn-ajout1" type="submit" class="adm" name="action"  value="Ajouter une demonstration" />
                <input id="inp-ret1" type="submit" name="action" value="Retour au menu principal" >
            </form>
            <?php foreach ($dVueDemonstrations as $tp => $questions){ ?> 

            <section id="<?php echo 'c'.$tp; ?>">
              <div class="container">
                <div class="row">
                  <div class="col-lg-8 mx-auto">
                    <h2 style = "text-transform:uppercase;"><?php echo $tp; ?></h2>

                    <form  class="demoForm" method="get" >
                            <?php $i=0;
                            foreach ($questions as $q) {
                               ?>
                            <?php  $i++;  echo $q->getNumQuestion() .'.'.$q->getQuestion();  $_SESSION['num']=array();  $_SESSION['num'][$i] =$q->getNumQuestion();  ?> <br/>
                                <textarea rows="5" cols="70" name="demo<?php echo $q->getNumQuestion(); ?>" id="demo<?php echo $q->getNumQuestion().$q->getNumTp(); ?>" >
                                    <?php echo $q->getReponse(); ?>
                                </textarea>
                               <p>
                                    <input class="submitDemoData" type="button" value="Exécuter" onclick="SubmitDemoData(<?php echo $q->getNumQuestion(); ?>,<?php echo $q->getNumTp(); ?>);" >
                                    <span id="inp-centre"> <input class="modifierDemonstration adm" type="button" value="Modifier" onclick="ModifierDemonstration(<?php echo $q->getNumQuestion(); ?>,<?php echo $q->getNumTp(); ?>);" >
                                        <input class="supprimerDemonstration adm" type="button" value="Supprimer" onclick="SupprimerDemonstration(<?php echo $q->getNumQuestion(); ?>,<?php echo $q->getNumTp(); ?>);" ></span>
                                    <span id="inp-gauche"><input class="monterDemonstration adm" type="button" value="Monter" onclick="MonterDemonstration(<?php echo $q->getNumQuestion(); ?>,<?php echo $q->getNumTp(); ?>)" >                                    
                                        <input class="descendreDemonstration adm" type="button" value="Descendre" onclick="DescendreDemonstration(<?php echo sizeof($questions); ?>,<?php echo $q->getNumQuestion(); ?>,<?php echo $q->getNumTp(); ?>)" ></span>
                                    
                                </p>

                            <div id="results<?php echo $q->getNumQuestion().$q->getNumTp(); ?>">
                                <!-- les résultats s'affichent içi -->
                            </div><br/>
                            <?php $i++; } //fin foreach ?>
                    </form>

                  </div>
                </div>
              </div>
            </section> <?php } ?>
           <!-- <form method="get" id="frm-reset">
                <input type="submit" name="action" id="regenererDemo" value="Reintiliser les demonstrations"  /><br/>
            </form>-->

            <form method="get" >
                <input id="btn-ajout" type="submit" class="adm" name="action"  value="Ajouter une demonstration" />
                <input id="inp-ret" type="submit" name="action" value="Retour au menu principal" >
            </form>

            <form method="get" role="form">
                <input type="submit" name="action" id="afficher-demo" value="Afficher les demonstrations" style='display:none'>

            </form>
           
            <form id="frm-export" class="adm">
                <input type="button" value="Exporter" onclick="ExporterDemonstrations()" />
            </form>
            <input type="text" id="verif-ad" value="<?php echo $_SESSION['typeSession'];?>" hidden/>
            <script>
                
                function scrollToTop() { 
                    window.scrollTo(0, 0); 
                } 
                
                $( document ).ready(function() {
                    var ad = $('#verif-ad').val();

                    if(ad == 'LJLf1')
                        $( ".adm").show();
                    else $( ".adm").remove();
                
                });
                
                
                function SubmitDemoData(numQuestion,numChapitre) {
                    //document.write($('#demo'+numQuestion).val());
                    var txt = $('#demo'+numQuestion+numChapitre).val() ; 
                    $.get("Traitement/AffichageDemo.php", { textbox: txt},
                    function(data) {
                         $('#results'+numQuestion+numChapitre).html(data);
                         //$('#demoForm')[0].reset();
                      });
                } 

                function MonterDemonstration(numDemo, numChapitre) {
                    if(numDemo > 1){
                        $.get("Traitement/OrdreDemo.php", {  numDemo : numDemo, numChapitre : numChapitre },
                        function(data) {

                             //$('#questionsForm')[0].reset();
                             //$('#erreur').html(data);
                             $("#afficher-demo").trigger("click");
                             //window.location.replace("index.php");
                          });
                    }
                }
                function DescendreDemonstration(nbQuestions,numQuestion, numTp) {
                //alert(nbQuestions);
                
                    var des = 1;
                    if(numQuestion < nbQuestions){
                        $.get("Traitement/OrdreDemo.php", { des : des , numDemo : numQuestion, numChapitre : numTp },
                        function(data) {

                             //$('#questionsForm')[0].reset();
                            $("#afficher-demo").trigger("click");
                             //window.location.replace("index.php");
                          });
                    }
                }
                
                function SupprimerDemonstration(numDemo,numChapitre) {
                    var result = confirm("Êtes-vous sûr de vouloir supprimer la démonstration "+numDemo+" du chapitre "+numChapitre+" ?");
                    if(result){
                        var type = "demo";
                        $.get("Traitement/Suppression.php", {  numChapitre : numChapitre, numDemo : numDemo,demo : type},
                        function(data) {
                             //$('#results'+numDemo+numChapitre).html(data);
                             //$('#questionsForm')[0].reset();
                               $("#afficher-demo").trigger("click");
                          });
                      }
                } 
            
                /*$(document).ready(function() {
                    var tps = $('#tp_keys').val();
                    var i = 0;
                    tpk = tps.split(",");
                    tpk.forEach(function(entry) {
                        if(i==0) $("#c"+entry).show();
                        else $("#c"+entry).hide();
                        i++;
                        console.log(entry);
                    });                              
                });*/
                function changerSection(tp,keys){

                    tpk = keys.split(",");
                    tpk.forEach(function(entry) {
                        $("#c"+entry).hide();
                        console.log(entry);
                    });               
                    $("#c"+tp).show();

                }

                function ModifierDemonstration(numDemo, numChapitre) {
                    $.get("Traitement/ModificationDemonstration.php", {  numDemo : numDemo, numChapitre : numChapitre },
                    function(data) {
                        $('#vued').empty();
                         $('#vued').html(data);
                         //$('#questionsForm')[0].reset();
                         //location.reload();
                      });                     
                }
                
                function ExporterDemonstrations(){
                    type = 'demo';
                    $.get("Traitement/Export.php", { type:type}, function(data) {
                        alert(data+"Les Demonstrations ont été exportées vers ;/var/www/html/Methodologie/imports/demonstration/demo_exportes.txt");
                    });
                }
            
                window.addEventListener('scroll',function() {       
                    localStorage.setItem('scrollPosition',window.scrollY);
                },false); 
                window.addEventListener('load',function() {
                    if(localStorage.getItem('scrollPosition') !== null)
                    window.scrollTo(0, localStorage.getItem('scrollPosition'));
                },false);
                    
            </script>
        </div>
        <!-- Footer -->
        <footer class="py-5 bg-dark">
          <div class="container">
            <p class="m-0 text-center text-white">IUT Clermont-Ferrand 2020</p>
          </div>           
        </footer>       
    </body>
</html>
