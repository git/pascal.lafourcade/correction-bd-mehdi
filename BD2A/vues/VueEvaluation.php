<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/VuePrincipale.css">
    </head>
    <body>       
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
           <div class="container">
             <a class="navbar-brand js-scroll-trigger" href="#page-top">TP de Base De Données</a>
             <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
             </button>
             <div class="collapse navbar-collapse" id="navbarResponsive">
               <ul class="navbar-nav ml-auto">
                 <li class="nav-item">
                   <a class="nav-link js-scroll-trigger" href="#TP2">TP2</a>
                 </li>
                 <li class="nav-item">
                   <a class="nav-link js-scroll-trigger" href="#TP3">TP3</a>
                 </li>
                 <li class="nav-item">
                   <a class="nav-link js-scroll-trigger" href="#TP4">TP4</a>
                 </li>
                 <li class="nav-item">
                   <a class="nav-link js-scroll-trigger" href="#TP5">TP5</a>
                 </li>
               </ul>
             </div>
           </div>
         </nav>
        <header class="bg-primary text-white">
          <div class="container text-center">
            <h1>test</h1>
            <p class="lead">TEST</p>
          </div>
        </header>

        <div style="font-weight: bold" id="Temps-Restant"></div>

        <section id="TP2">
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <h2>TP 2</h2>
                <form  method="post" action="Correcteur.php" name="Exam">
                        <?php $i=0;

                        foreach ($dVueQuestions as $q) { ?>
                        <p><?php  $i++;  echo $q->getNumQuestion() .'.'.$q->getQuestion();  $_SESSION['num']=array();  $_SESSION['num'][$i] =$q->getNumQuestion();  ?> <br/>
                            <textarea rows="5" cols="70" name="textbox[]" >
                                <?php echo (isset($_SESSION['fi'][$i]) ? $_SESSION['fi'][$i] : '') ?>
                            </textarea>
                            <input>
                            <strong>TEST</strong>
                        </p>
                            
                        <?php } //fin foreach ?>
                        <p><input type="submit" ></p>


                    </form>
              </div>
            </div>
          </div>
        </section>
        <form method="post" class="frm">
            <input type="submit" name="test" id="test" value="Générer Base de données aléatoire" /><br/>
            <input type="submit" name="regenerer" id="test" value="Réintiliser les questions" /><br/>
        </form>
        <a href="vues/VueAdmin.php"><button>Ajouter Une Question</button></a>


        <?php

        if(array_key_exists('test',$_POST)){
           $oraDb = new OracleDb;
        }
       if(array_key_exists('regenerer',$_POST)){
           $db = new SqliteDb();
           $db->createTable();
           $URL="index.php";
           echo '<META HTTP-EQUIV="refresh" content="0;URL=' . $URL . '">';
        }

        ?>
        
       <script type="text/javascript">
            var tempsMin =50 ;
            var total_secondes =60*tempsMin;
            tempsMin = parseInt(total_secondes/60);
            secondes = parseInt(total_secondes%60);
            document.getElementById("Temps-Restant").innerHTML='Temps restant: ' + tempsMin + ' minutes ' + secondes + ' secondes';
            function init(){
                document.getElementById("Temps-Restant").innerHTML='Temps restant: ' + tempsMin + ' minutes ' + secondes + ' secondes';
                setTimeout("TempsRestant()",999);
            }
            function TempsRestant(){
                document.getElementById("Temps-Restant").innerHTML='Temps restant: ' + tempsMin + ' minutes ' + secondes + ' secondes' ;
                if(total_secondes <=0){
                    setTimeout('document.Exam.submit()',1);
                } 
                else {
                    total_secondes = total_secondes -1;
                    tempsMin = parseInt(total_secondes/60);
                    secondes = parseInt(total_secondes%60);
                    setTimeout("TempsRestant()",999);
                }
            }
            init(); 
        </script>
        <!-- Footer -->
        <footer class="py-5 bg-dark">
          <div class="container">
            <p class="m-0 text-center text-white">IUT Clermont-Ferrand 2020</p>
          </div>
          <!-- /.container -->
        </footer>

        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom JavaScript for this theme -->
        <script src="js/scrolling-nav.js"></script>
         
    </body>
</html>
