 
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script src="css/jquery-3.5.1.js"></script>
        <link rel="stylesheet" href="css/VueAdmin.css">
        
    </head>

    <body>
        <form  style="border:1px solid #ccc" method="GET" action="../Traitement/AjoutQuestion.php">
            <div class="container">


            <h1>Ajouter une question</h1>
            <hr>

            <label><b>TP n° : </b></label>
            <input type="text" placeholder="Numéro du TP" id="tp" name='tp' required>

            <label><b>Barème : </b></label>
            <input type="text" placeholder="Barème..." id="points" required>

            <label><b>Consigne : </b></label>
            <textarea placeholder="Ecrire la consigne..." id="consigne" required></textarea> 

            <label><b>Réponse : </b></label>
            <textarea placeholder="SELECT ..." id="reponse" required></textarea> <br/><br/>

            <input class="executerReponse" type="button" value="Exécuter" onclick="ExecuterReponse();" > <br/><br/>
            <div id="execute">
            
            </div> <div><br/><br/><br/></div>
            
            <label><b>Verification avec BDD aléatoire? </b><br/>
                <input type="radio"  name="aleatoire" value="o" style="margin-bottom:15px" checked> oui
                <input type="radio"  name="aleatoire" value="n" style="margin-bottom:15px"> non
            </label><br/>
            


            <div >
                <input type="button" value="Ajouter"  onclick="SubmitAjoutQuestion()" />
            </div>
          </div>
        </form>
        <div id="erreur">
            
        </div>
        
         <form method="get" id="frm-demo">
            <input id="inp-demo" class="bouton" type="submit" name="action" value="Afficher les questions" > 
        </form><br/>
        
        <input type="file" id="fileInput"> <br/>
        <label>Requête sous forme : <strong> type(requete\fonction)**NBA**NumeroDeQuestion**Consigne**requete(SELECT...)**barème**verificationAvecBddAleatoire(o\n)**fonction(create function ...) -optionnel</strong> </label>
        <p> Tous les noms de table doivent être en majuscule</p>

    </body>    
       
    <script>
        
        $(document).ready(function() {
            $('input[type="radio"]').click(function() {
                if($(this).attr('id') == 'btn-fonction') {
                     $('#show-me').show();           
                }

                else {
                     $('#show-me').hide();   
                }
            });
        });

        function ExecuterReponse() {
                    //document.write($('#demo'+numQuestion).val());
                    var tx = $('#reponse').val() ;
                    var txt =  tx.replace(/^\s+/g, "");

                    txt = txt.trim();

                    if(!(txt.toUpperCase().includes('CREATE') && txt.toUpperCase().includes('FUNCTION'))) {
                        if (txt.charAt(txt.length - 1) == ';') {
                          txt = txt.substr(0, txt.length - 1);
                        }
                    }
                    $.get("Traitement/AffichageDemo.php", { textbox: txt},
                    function(data) {
                          $('#execute').empty();
                            setTimeout( function(){ $('#execute').html(data);  }  , 400 );  
                         //$('#demoForm')[0].reset();
                      });
        } 

        function SubmitAjoutQuestion(mot = 0) {
                    //document.write($('#demo'+numQuestion).val());
                    $('#erreur').empty();
                    wait(1500);
                    
                    if(mot == 0){
                        var tp = $('#tp').val() ; 
                        var points = $('#points').val() ; 
                        var consigne = $('#consigne').val() ; 
                        var reponse = $('#reponse').val() ; 
                        var type = $("input[name='type']:checked").val(); 
                        var type = 'requete';
                        var aleatoire= $("input[name='aleatoire']:checked").val(); 
                         //var bdd = $("input[name='bdd']:checked").val();
                        var bdd = 'NBA';
                        var fonction = $('#fonction').val() ;                 
                    }
                    else {
                        var type = mot[0];
                        var bdd = mot[1];
                        var tp = mot[2];
                        var consigne = mot[3];
                        var reponse = mot[4];                      
                        var points = mot[5];
                        var aleatoire = mot[6];
                        var fonction = mot[7];
                        if(fonction != null){
                            //alert(type+'-'+bdd+'-'+tp+'-'+num+'-'+consigne+'-'+reponse+'-'+points+'-'+aleatoire+'-'+fonction);
                            //alert()
                            //alert('ok');
                        }
                        //else alert(type+'-'+bdd+'-'+tp+'-'+num+'-'+consigne+'-'+reponse+'-'+points+'-'+aleatoire);
                        
                    }
                    
                    var tx = reponse;
                    var txt =  tx.replace(/^\s+/g, "");

                    txt = txt.trim();

                    if(!(txt.toUpperCase().includes('CREATE') && txt.toUpperCase().includes('FUNCTION'))) {
                        if (txt.charAt(txt.length - 1) == ';') {
                          txt = txt.substr(0, txt.length - 1);
                        }
                    }
                    reponse = txt; 
                   $.get("Traitement/AjoutQuestion.php", { tp: tp,points: points,consigne: consigne,reponse: reponse,type: type,aleatoire: aleatoire,bdd: bdd, fonction : fonction},
                    function(data) {
                         if(mot != 0){                          
                            $('#erreur').append('<div class="err">'+data+'</div>');                             
                        }
                        else{
                            $('#erreur').empty();
                            setTimeout( function(){ $('#erreur').html(data);  }  , 400 );                        
                        }
                         //$('#demoForm')[0].reset();
                      });            
        }
        
        function wait(ms){
            var start = new Date().getTime();
            var end = start;
            while(end < start + ms) {
              end = new Date().getTime();
            }
        }
        
        var fileInput = document.getElementById('fileInput');
        var fileDisplayArea = document.getElementById('fileDisplayArea');
        var mot = [];
        fileInput.addEventListener('change', function(e) {
            var file = fileInput.files[0];
            var textType = /text.*/;

            if (file.type.match(textType)) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                    var content = reader.result;
                    //Here the content has been read successfuly
                    //alert(content);
                    
                    var lignes = content.split('\n');
                    for(var line = 0; line < lignes.length; line++){
                        var mot = lignes[line].split('**');
                        if (mot != "") SubmitAjoutQuestion(mot);
                        //mots.push(mot);        
                        
                        console.log(mot[0]);console.log(mot[1]);console.log(mot[2]);console.log(mot[3]);
                    }
                    //$('#inp-questions').trigger("click");
                }

                reader.readAsText(file);	
            } else {
                fileDisplayArea.innerText = "fichier non supporté"
            }
        });



    </script>
</html>
