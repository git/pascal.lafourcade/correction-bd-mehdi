<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8"><style><?php global $rep,$vues,$css; include $rep.$css['bootstrap']; ?></style>
        <title></title>
        <script src="css/jquery-3.5.1.js"></script>
        
        <link rel="stylesheet" href="css/VueAccueil.css">
    </head>
    <body id="vuep">       
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
           <div class="container">
             <a class="navbar-brand js-scroll-trigger" href="#page-top">Base De Données</a>
             <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
             </button>             
           </div>
         </nav>
        <header class="bg-primary text-white">
          <div class="container text-center">
            
            <p class="lead"></p>
          </div>
        </header>
        <div id='allp'>

            <form method="get" role="form">
                <input class="bouton" type="submit" name="action" value="Demonstrations de cours" ><br/>
                <input class="bouton" type="submit" name="action" value="QCM" ><br/>
                <input class="bouton" type="submit" name="action" value="TP"><br/>
                <input class="bouton adm" type="submit" name="action" value="Creation de table"><br/>
                <input class="bouton adm" type="submit" name="action" value="Gestion de tables"><br/>
            </form>

           
<input type="text" id="verif-ad" value="<?php echo $_SESSION['typeSession'];?>" hidden/>
        </div>      

        <!-- Footer 
        <footer class="py-5 bg-dark">
          <div class="container">
            <p class="m-0 text-center text-white">IUT Clermont-Ferrand 2020</p>
          </div>
          
        </footer>-->

        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom JavaScript for this theme -->
        <script src="js/scrolling-nav.js"></script>
         
    </body>
    
    <script>
        $( document ).ready(function() {
                    var ad = $('#verif-ad').val();
                   
                    if(ad == 'LJLf1')
                        $( ".adm").show();
                    else $( ".adm").remove();
                
                });
    </script>
</html>
