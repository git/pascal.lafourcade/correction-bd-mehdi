<?php

class Controleur {
    
    function __construct() {
        global $rep,$vues;
        session_start();
        
         $dVueEreur = array ();
         if($this->isAdmin())
            $_SESSION['typeSession'] = 'LJLf1';
         else $_SESSION['typeSession'] = 'user';
        try{
            $action=$_REQUEST['action'];
         // echo $action;
            switch($action) {
          
                case NULL :
                        //require ($rep.$vues['vueLogin']);
                    //$this->afficherQuestions(); 
                    $this->afficherQCM();
                    break;

                case 'Retour au menu principal' :
                        //require ($rep.$vues['vueLogin']);
                    //$this->afficherQuestions(); 
                    require ($rep.$vues['vueAccueil']);
                    break;

                case 'Dates de TP' : 
                    require ($rep.$vues['vueDateTp']);
                    break;

                case 'Ajouter une question' : 
                    require ($rep.$vues['vueAdmin']);
                    break;

                case 'TP':
                        //require ($rep.$vues['vueLogin']);
                    $this->afficherQuestions(); 
                        break;
                case 'Afficher les questions':
                        //require ($rep.$vues['vueLogin']);
                    $this->afficherQuestions(); 
                        break;
                case 'login' :
                    //$this->verificationConnexion();
                    require ($rep.$vues['vueLogin']);
                    break;
                case 'Valider' :              
                    $this->Correction();
                    break;

                case 'Demonstrations de cours' :          
                    $this->afficherDemonstrations();
                    break;

                case 'Afficher les demonstrations' :
                    //$this->verificationConnexion();      
                    $this->afficherDemonstrations();
                    break;

                case 'Ajouter une demonstration' : 
                    require ($rep.$vues['vueAjoutDemo']);
                    break;

                case 'Générer Base de données aléatoire':
                    $this->resetRandomBdd();
                    break;

                case 'ok' :           
                    $this->afficherDemonstrations();
                    break;

                case 'Reintiliser les demonstrations' :
                    $this->resetDemonstrations();
                    break;

                case 'Reintiliser les QCM' :
                    $this->resetQCM();
                    break;

                case 'Réintiliser les questions' :
                    $this->resetQuestions();
                    break;

                case 'QCM' :                
                    $this->afficherQCM();
                    break;

                case 'Ajouter QCM' : 
                    require ($rep.$vues['vueAjoutQCM']);
                    break;

                case 'Creation de table' :                
                    $this->afficherCreationTable();
                    break;

                case 'Gestion de tables' :                
                    $this->afficherGestionTable();
                    break;

                default:
                    $dVueEreur[] ="Erreur d'appel php";           
                    require ($rep.$vues['erreur']);
                    break;

          }

        } catch (PDOException $e)
        {
            $dVueEreur[] = "Erreur inattendue!!! ";
            //require ($rep.$vues['erreur']);
        }
        
        
        //$this->afficherQuestions();
    }    
    
    
    function isAdmin(){
        switch($_SERVER['REMOTE_USER']){
            case 'meelaichao':
                return true;
                break;
            case 'palafour' :
                return true;
                break;
            default :
                return false;
        }
        
    }
    function resetQCM(){
        $db = new SqliteDb();
        $db->createQCM();
        $db->createNotes();
        $db->creerSauvegardeQcm();
        $this->afficherQCM();
    }
    
    function resetQuestions(){
        $db = new SqliteDb();
        $db->createTable();
        $this->afficherQuestions();
    }
    function resetRandomBdd(){
        require_once('BDD/OracleDb.php');
        $oraDb = new OracleDb();
        $oraDb->createRandomTables();
        $this->afficherQuestions();
    }
    function resetDemonstrations(){
        $db = new SqliteDb();
        $db->createDemonstration();
        $this->afficherDemonstrations();
        
    }
    
    function afficherQuestions(){
        global $rep,$vues,$css;
        
        $model = new Modele();
        $dVueQuestions = $model->afficherQuestions();
        $_SESSION['array']=$dVueQuestions;
        //require ($rep.$vues['vueSupression']); 
        require ($rep.$vues['vuePrincipale']); 
        //require ($rep.$vues['vueNbQuestions']); 
        //session_destroy();
    }
    
    function afficherDemonstrations(){
        global $rep,$vues;
        
        $model = new Modele();
        $dVueDemonstrations = $model->afficherDemonstrations();
        $_SESSION['arrayDemo']=$dVueDemonstrations;
        //require ($rep.$vues['vueSupression']); 
        //require ($rep.$vues['vuePrincipale']); 
        require ($rep.$vues['vueDemonstration']); 
        //session_destroy();
    }
    
    function afficherQCM(){
        global $rep,$vues;
        
        $model = new Modele();
        $tabQCM = $model->afficherQCM();
        $_SESSION['arrayQCM']=$tabQCM;

        require ($rep.$vues['vueQCM']); 
        //session_destroy();
    }
    
    function verificationConnexion(){
         global $rep,$vues;
         
         $id = $_POST['identifiant'];
         $mdp = $_POST['motdepasse'];
         
         $modele = new Modele();
         if($modele->connection($id,$mdp)==null){
            require($rep.$vues['vueLogin']);
        }
        else $this->afficherQuestions();              
    }
    
    function Correction(){
        require('Correcteur.php');
    }
    
    function afficherCreationTable(){
                global $rep,$vues;

        require ($rep.$vues['vueLibre']);
    }
    
    function afficherGestionTable(){
        global $rep,$vues;

        require ($rep.$vues['vueGestion']);
    }
}

