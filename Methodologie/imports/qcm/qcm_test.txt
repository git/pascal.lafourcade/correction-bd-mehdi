3**Test d'Epworth**test**Pendant que vous êtes occupé à lire un document**0---0+++1---1+++2---2+++3---3
3**Test d'Epworth**test**Devant la télévision ou au cinéma**0---0+++1---1+++2---2+++3---3
3**Test d'Epworth**test**Assis inactif dans un lieu public(salle d’attente, théâtre,…)**0---0+++1---1+++2---2+++3---3
3**Test d'Epworth**test**Passager, depuis au moins une heure sans interruptions, d’une voiture ou d’un transport en commun(train, bus, avion, …)**0---0+++1---1+++2---2+++3---3
3**Test d'Epworth**test**Allongé pour une sieste, lorsque les circonstances le permettent**0---0+++1---1+++2---2+++3---3
3**Test d'Epworth**test**En position assise au cours d’une conversation (ou au téléphone) avec un proche**0---0+++1---1+++2---2+++3---3
3**Test d'Epworth**test**Tranquillement assis à table à la fin d’un repas sans alcool**0---0+++1---1+++2---2+++3---3
3**Test d'Epworth**test**Au volant d’une voiture immobilisée depuis quelques minutes dans un embouteillage**0---0+++1---1+++2---2+++3---3
4**Accro**test**Passez-vous plus de temps sur votre smartphone que vous ne le pensez ?**oui---1+++non---0
4**Accro**test**Vous arrive-t-il de passer régulièrement du temps sans réfléchir en regardant votre smartphone ?**oui---1+++non---0
4**Accro**test**Avez-vous l’impression de perdre la notion du temps lorsque vous êtes sur votre smartphone ?**oui---1+++non---0
4**Accro**test**Passez-vous plus de temps à envoyer des textos, des tweets ou des emails qu’à parler aux gens en personne ?**oui---1+++non---0
4**Accro**test**Passez-vous de plus en plus de temps sur votre smartphone ?**oui---1+++non---0
4**Accro**test**Souhaiteriez-vous passer moins de temps à utiliser votre smartphone ?**oui---1+++non---0
4**Accro**test**Dormez-vous régulièrement avec votre smartphone allumé sous votre oreiller ou près de votre lit ?**oui---1+++non---0
4**Accro**test**Vous arrive-t-il de regarder et de répondre à des textos, des tweets et des emails à toute heure du jour et de la nuit, même si cela signifie interrompre d’autres choses que vous êtes en train de faire ?**oui---1+++non---0
4**Accro**test**Envoyez-vous des textos, des emails, des tweets, des messages Snapchat, des messages Facebook ou surfez-vous en conduisant ou en faisant d’autres activités semblables qui exigent votre attention et votre concentration ?**oui---1+++non---0
4**Accro**test**Avez-vous l’impression que l’utilisation de votre smartphone diminue parfois votre productivité ?**oui---1+++non---0
4**Accro**test**Vous sentez-vous réticent à rester sans votre smartphone, même pour une courte période de temps ?**oui---1+++non---0
4**Accro**test**Vous sentez-vous mal à l’aise lorsque vous laissez accidentellement votre smartphone dans la voiture ou à la maison, que vous n’avez pas de réseau ou que vous avez un smartphone cassé ?**oui---1+++non---0
4**Accro**test**Lorsque vous mangez, votre smartphone est-il toujours à portée de main à côté de vous à table ?**oui---1+++non---0
4**Accro**test**Lorsque votre smartphone sonne, émet des bips ou vibre, ressentez-vous une forte envie de vérifier les textos, les tweets, les emails, les mises à jour, et ainsi de suite?**oui---1+++non---0
4**Accro**test**Vous arrive-t-il de vérifier votre smartphone plusieurs fois par jour, même si vous savez qu’il n’y a probablement rien de nouveau ou d’important à voir ?**oui---1+++non---0
5**Audit**test**A quelle fréquence vous arrive-t-il de consommer des boissons contenant de l'alcool ?**Jamais---0+++Au moins une fois par mois---1+++2 à 4 fois par mois---2+++2 à 3 fois par semaine---3+++4 fois ou plus par semaine---4
5**Audit**test** Combien de verres standards buvez-vous au cours d'une journée ordinaire où vous buvez de l'alcool ?**1 ou 2---0+++3 ou 4---1+++4 ou 5---2+++7 ou 9---3+++10 ou plus---4
5**Audit**test**Au cours d'une méme occasion, combien de fois vous arrive-t-il de boire six verres standards ou plus**Jamais---0+++Moins d'une fois par mois---1+++1 fois par mois---2+++1 fois par semaine---3+++Tous les jours ou presque---4
5**Audit**test** Dans les 12 derniers mois, combien de fois avez-vous observé que vous n'étiez plus capable de vous arrêter de boire après avoir commencé**Jamais---0+++Moins d'une fois par mois---1+++1 fois par mois---2+++1 fois par semaine---3+++Tous les jours ou presque---4
5**Audit**test**Dans les 12 derniers mois, combien de fois le fait d'avoir bu de l'alcool, vous-a-t-il empêché de faire ce qu'on attendait normalemet de vous ?**Jamais---0+++Moins d'une fois par mois---1+++1 fois par mois---2+++1 fois par semaine---3+++Tous les jours ou presque---4
5**Audit**test**Dans les 12 derniers mois, combien de fois, après une période de forte consommation, avez-vous du boire de l'alcool dès le matin pour vous remettre en forme ?**Jamais---0+++Moins d'une fois par mois---1+++1 fois par mois---2+++1 fois par semaine---3+++Tous les jours ou presque---4
5**Audit**test**Dans les 12 derniers mois, combien de fois avez-vou eu un sentiment de culpabilité ou de regret après avoir bu ?**Jamais---0+++Moins d'une fois par mois---1+++1 fois par mois---2+++1 fois par semaine---3+++Tous les jours ou presque---4
5**Audit**test**Dans les 12 derniers mois, combien de fois avez vous été incapable de vous souvenir de ce qui s'était passé la nuit précédente parce que vous aviez bu ?**Jamais---0+++Moins d'une fois par mois---1+++1 fois par mois---2+++1 fois par semaine---3+++Tous les jours ou presque---4
5**Audit**test** Vous êtes-vous blessé ou avez-vous blessé quelqu'un parce que vous aviez bu ?**Non---0+++Oui mais pas dans l'année passée---2+++Oui au cours de l'année dernière---4
5**Audit**test**Est-ce qu'un parent, un ami, un médecin ou un autre professionnel de santé s'est déjà préoccupé de votre consommation d'alcool et vous a conseillé de la diminuer ?**Non---0+++Oui mais pas dans l'année passée---2+++Oui au cours de l'année dernière---4
5**Audit**test**Vous êtes :**Un homme---0+++Une femme---1
