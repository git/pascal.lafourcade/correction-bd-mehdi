<?php

session_start();
require_once('../BDD/SqliteDb.php');
require_once('../BDD/OracleDb.php');
$db = new SqliteDb('o');

$oracleDb = new OracleDb();
$conn = $oracleDb->getConn();
$numReponseRow = array();

//$user = "mehdi";
$user = $_SERVER['REMOTE_USER'];

if(!isset($_GET['type'])){
    $query = $db->prepare('SELECT numReponse FROM QcmCorrection WHERE numQuestion=? AND numQcm= ?');
    $query->bindParam(1, $_GET['numQuestion']);
    $query->bindParam(2, $_GET['numQcm']);
    $result = $query->execute();

    while($r = $result->fetchArray() ){
        $numReponseRow[] = $r['numReponse'];
    }

    sort($numReponseRow); 
    sort( $_GET['rep']); 


    if ($numReponseRow ==  $_GET['rep']){
        echo "<b>Bonne réponse ! </b>\n";
        
        $query = $db->prepare('INSERT OR REPLACE INTO NotesParQuestion  VALUES (?,?,?,?)');        
        $query->bindParam(1,$user);
        $query->bindParam(2,$_GET['numQcm']);
        $query->bindParam(3,$_GET['numQuestion']);
        $query->bindParam(4,$_GET['bareme']);
        $result = $query->execute();
                                         
    } 
        
    else{
        echo "<b>Mauvaise réponse.</b>\n"; 
        $zero = 0;
        $query = $db->prepare('INSERT OR REPLACE INTO NotesParQuestion  VALUES (?,?,?,?)');        
        $query->bindParam(1,$user);
        $query->bindParam(2,$_GET['numQcm']);
        $query->bindParam(3,$_GET['numQuestion']);
        $query->bindParam(4,$zero);
        $result = $query->execute();
        
    }
    
    $query = $db->prepare('SELECT note FROM NotesParQuestion WHERE numQcm= ? AND idEtudiant = ?');
    $query->bindParam(1, $_GET['numQcm']);
    $query->bindParam(2, $user);
    $result = $query->execute();

    $note = 0;
    while($r = $result->fetchArray()){
        $note += $r['note'];    
    }

    $query = $db->prepare('INSERT OR REPLACE INTO Notes  VALUES (?,?,?)');
    $query->bindParam(1,$user );
    $query->bindParam(2,$_GET['numQcm']);
    $query->bindParam(3,$note );
    $result = $query->execute();
   //$db->ecrireFichierNotes($_GET['numQcm']);
        
  
}


else{
    $query = $db->prepare('SELECT count(*) FROM QcmQuestion WHERE  numQcm= ? ');
    $query->bindParam(1, $_GET['numQcm']);
    $result = $query->execute();
    $nbQuestionRow = $result->fetchArray();
    
    $total = 0;
    for($i = 1 ; $i<=$nbQuestionRow['count(*)'] ; $i++ ){
        $query = $db->prepare('SELECT max(points) FROM QcmReponse WHERE  numQcm= ? AND numQuestion=?');
        $query->bindParam(1, $_GET['numQcm']);
        $query->bindParam(2, $i);
        $result = $query->execute();
        $totalRow = $result->fetchArray();
        
        $total += $totalRow['max(points)'];
    }
    
    $query = $db->prepare('SELECT * FROM QcmReponse WHERE  numQcm= ? ');
    $query->bindParam(1, $_GET['numQcm']);
    $result = $query->execute();
    $totalRow = $result->fetchArray();

    
    $points = 0;
    for ($i = 1 ; $i < sizeof($_GET['rep']) ; $i++){
        $query = $db->prepare('SELECT * FROM QcmReponse WHERE numQuestion=? AND numQcm= ? AND numReponse= ?');
        $query->bindParam(1, $i);
        $query->bindParam(2, $_GET['numQcm']);
        $query->bindParam(3, $_GET['rep'][$i][0]);
        $result = $query->execute();
        $pointRow = $result->fetchArray();
        $points +=$pointRow['points'];
    }    
    echo '<b>Résultat : '.$points.' sur '.$total.'</b>';
    
    
}



