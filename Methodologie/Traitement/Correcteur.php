<?php
ob_start();


session_start();
require_once('../BDD/SqliteDb.php');
require_once('../BDD/OracleDb.php');
header('Content-type: text/plain');
$db = new SqliteDb('o');

$n = new OracleDb();
//$conn = oci_connect('u_prems', '123456','localhost/orcl');
//$conn = oci_connect('meelaichao', 'meelaichao', 'kirov:1521/kirov');
$conn = $n->getConn();

$_SESSION['fi'] = array();
$cmpt = 0;
$numQuestion = 0;
header("Cache-Control: no cache");


    $textbox = $_GET['textbox'];
   /*$numQuestion++;
    $ic = "$numQuestion";*/
    
   $numQuestion = $_GET['numQuestion'];
   $numTp = $_GET['numTp'];
    $_SESSION['fi'][$numQuestion] = $textbox;
    $UserQuery = oci_parse($conn, $textbox);
    $reponseUser = oci_execute($UserQuery);
    
    $typeQuery = $db->prepare('SELECT type FROM Type WHERE numQuestion = ? AND numTp= ?');
    $typeQuery->bindParam(1, $numQuestion);
    $typeQuery->bindParam(2, $numTp);
    $resultType = $typeQuery->execute();
    $typeRow = $resultType->fetchArray();
    $err = 0;
    $verif =0;
    
    //si la question attend un SELECT
    
    if($typeRow['type'] == 'query'){
        // <editor-fold desc="vérification avec la vraie BDD">
        
        $sqliteQuery = $db->prepare('SELECT reponse FROM Correct WHERE numquestion= ?  AND numTp= ? ');
        $sqliteQuery->bindParam(1, $numQuestion);
        $sqliteQuery->bindParam(2, $numTp);
        
        $result = $sqliteQuery->execute();
        $sqliteRow = $result->fetchArray();
        
        
        $vraiReponse = oci_parse($conn, $sqliteRow['reponse']);
        $enAttente = oci_execute($vraiReponse);  
        
        if(oci_num_rows($UserQuery) != oci_num_rows($vraiReponse)){
            $err=-3;
        }
        while( ($oracleRow = oci_fetch_array($UserQuery, OCI_NUM)) && $err == 0 ){              
            $vraiReponseRow = oci_fetch_array($vraiReponse,OCI_NUM );
            $verif = 1;
            if(sizeof($oracleRow) == sizeof($vraiReponseRow)){
                
                for($i=0 ; $i< sizeof($vraiReponseRow) ; $i++){
                    if($oracleRow[$i] != $vraiReponseRow[$i]){
                        $err = -1;
                        break;
                    }               
                }
            }
            else {
                $err = -2;
                break;
            }
        }
        //echo nl2br("\n ------------test : ".$err."   test2 : ".$verif."\n");
        // </editor-fold>

        $aleatoireQuery = $db->prepare('SELECT aleatoire FROM Correct WHERE numQuestion = ?  AND numTp= ?');
        $aleatoireQuery->bindParam(1, $numQuestion);
        $aleatoireQuery->bindParam(2, $numTp);
        $resultAleatoire = $aleatoireQuery->execute();
        $aleatoireRow = $resultAleatoire->fetchArray();
        if($aleatoireRow['aleatoire'] == 'o'){
            // <editor-fold desc="vérification avec bdd aléatoire"> 
            $nv= $sqliteRow['reponse'];   
            $txt = $textbox;

            if(strpos($sqliteRow['reponse'],"GAME")){
                $nv = str_replace('GAME', ' randomgame ', $nv);
                $txt = str_replace('GAME', 'randomgame', $txt);

            }
            if (strpos($sqliteRow['reponse'],"EQUIPE")){
                $nv= str_replace('EQUIP', ' randomequipe ', $nv);
                $txt = str_replace('EQUIPE', 'randomequipe', $txt);
            }
            if (strpos($sqliteRow['reponse'],"STATS")){
                $nv=  str_replace('STATS' , ' randomstats ', $nv);
                $txt = str_replace('STATS', ' randomstats ', $txt);
            }
            if (strpos($sqliteRow['reponse'],"JOUEUR")) {
                $nv =    str_replace('JOUEUR', 'randomjoueur', $nv);
                $txt = str_replace('JOUEUR', 'randomjoueur', $txt);
            }                

            $UserQueryRandom = oci_parse($conn, $txt);
            $reponseUser = oci_execute($UserQueryRandom);       

            $vraiReponseRandom = oci_parse($conn, $nv);
            $enAttente = oci_execute($vraiReponseRandom);
            if(oci_num_rows($UserQueryRandom) != oci_num_rows($vraiReponseRandom)){
                $err=-3;
            }
            $verif = 0;
            while( ($oracleRowRandom = oci_fetch_array($UserQueryRandom, OCI_NUM)) && $err == 0 ){              
                $vraiReponseRowRandom = oci_fetch_array($vraiReponseRandom,OCI_NUM );
                $verif = 1;

                if(sizeof($oracleRowRandom) == sizeof($vraiReponseRowRandom)){               
                    for($i=0 ; $i< sizeof($vraiReponseRowRandom) ; $i++){
                        if($oracleRowRandom[$i] != $vraiReponseRowRandom[$i]){
                            $err = -1;
                            break;
                        }               
                    }
                }
                else {
                    $err = -2;
                    break;
                }
            }     
            // </editor-fold>
            //echo nl2br("\n ------------testerr : ".$err."   test2verif : ".$verif."\n");
        }
        // <editor-fold desc="Résultat">
        if($err == 0 && $verif==1){
            echo "<strong>La réponse à la question " .$numQuestion. " est JUSTE ! \n</strong>";
            $cmpt++;
        }   
        else echo "<strong>La réponse à la question " .$numQuestion. " est FAUSSE ! \n</strong>";
        //</editor-fold>
    }
    //si la question attend une modification
    else if($typeRow['type'] == 'tablemodification'){
        
        $sqliteQuery = $db->prepare('SELECT reponse FROM Correct WHERE numquestion= ?  AND numTp= ? ');
        $sqliteQuery->bindParam(1, $numQuestion);
        $sqliteQuery->bindParam(2, $numTp);
        $result = $sqliteQuery->execute();
        $sqliteRow = $result->fetchArray();
        $vraiReponse = oci_parse($conn, $sqliteRow['reponse']);
        $enAttente = oci_execute($vraiReponse); 
        
        $sqliteQuery = $db->prepare('SELECT fonctioncorrect FROM Correct WHERE numquestion= ?  AND numTp= ?');
        $sqliteQuery->bindParam(1, $numQuestion);
        $sqliteQuery->bindParam(2, $numTp);
        $result = $sqliteQuery->execute();
        $sqliteRow = $result->fetchArray();
        
        
        $t = oci_fetch_all($vraiReponse, $vraiReponseRow);       
        print_r($vraiReponseRow);
        
        $err = 1;
        foreach ($vraiReponseRow as $v){
            for($i = 0 ; $i < sizeof($v, OCI_NUM) ; $i++){               
                if($v[$i] ==  $sqliteRow[0]){
                    $err = 0;
                }
            }
        }
        
        if($err == 0 ){
            echo "La réponse à la question " .$numQuestion. " est JUSTE ! \n";
            $cmpt++;
        }
        else echo "La réponse à la question " .$numQuestion. " est FAUSSE ! \n";
        
        
    }
    //si la question attend un CREATE FUNCTION
    else{
        $fonctionQuery = $db->prepare('SELECT * FROM FunctionCorrect WHERE numquestion= ?  AND numTp= ? ');
        $fonctionQuery->bindParam(1, $numQuestion);
        $fonctionQuery->bindParam(2, $numTp);
        $result = $fonctionQuery->execute();
        $fonctionRow = $result->fetchArray();
        
        $creationFonction = oci_parse($conn, $fonctionRow['fonctionCorrect']);
        $creatF = oci_execute($creationFonction);

        $testUser = oci_parse($conn, $fonctionRow['testUser']);
        $testu = oci_execute($testUser);        
        $testF = oci_parse($conn, $fonctionRow['reponse']);
        $testf = oci_execute($testF);
        
        if(oci_num_rows($testUser) != oci_num_rows($testF)){
                $err=-3;
        }
        while( ($reponseRow = oci_fetch_array($testUser,OCI_NUM)) && $err == 0){     

            $vraiReponseRow = oci_fetch_array($testF,OCI_NUM );
            
            $verif=1;
            if(sizeof($reponseRow) == sizeof($vraiReponseRow)){            
                for($i=0 ; $i< sizeof($vraiReponseRow) ; $i++){ 
                    //echo nl2br('reponse : '.$reponseRow[$i].'question : '.$vraiReponseRow[$i]);
                    if($reponseRow[$i] != $vraiReponseRow[$i]){
                        $err = -1;
                        break;
                    }    
                }
            }
            else {
                $err = -2;
                break;
            }
        }      
        
        // <editor-fold desc="vérification avec bdd aléatoire"> 

        $nv = $fonctionRow['fonctionCorrect'];
        $txt = $textbox;      
        
        if(strpos($fonctionRow['fonctionCorrect'],"GAME")){
            $nv = str_replace('GAME', ' randomgame ', $nv);
            $txt = str_replace('GAME', 'randomgame', $txt);
        }
        if (strpos($fonctionRow['fonctionCorrect'],"EQUIPE")){
            $nv= str_replace('EQUIPE', ' randomequipe ', $nv);
            $txt = str_replace('EQUIPE', 'randomequipe', $txt);
        }
        if (strpos($fonctionRow['fonctionCorrect'],"STATS")){
            $nv=  str_replace('STATS' , ' randomstats ', $nv);
            $txt = str_replace('STATS', ' randomstats ', $txt);
        }
        if (strpos($fonctionRow['fonctionCorrect'],"JOUEUR")) {
            $nv = str_replace('JOUEUR', 'randomjoueur', $nv);
            $txt = str_replace('JOUEUR', 'randomjoueur', $txt);
        }  
        
        $UserFonctionRandom = oci_parse($conn, $txt);
        $reponseUserRandom = oci_execute($UserFonctionRandom);
        //echo 'bbbb'.$txt;
        $creationFonctionRandom = oci_parse($conn, $nv);
        $creatFRandom = oci_execute($creationFonctionRandom);
        //echo '154789'.$nv;
      
        $testUserRandom = oci_parse($conn, $fonctionRow['testUser']);
        $testuR = oci_execute($testUserRandom);        
        //echo 'aaa'.$fonctionRow['testUser'];
        $testFR = oci_parse($conn, $fonctionRow['reponse']);
        $testfR = oci_execute($testFR);
        
        
        if(oci_num_rows($testUserRandom) != oci_num_rows($testFR)){
                $err=-3;
        }
        $verif=0;
        while( ($reponseRowRandom = oci_fetch_array($testUserRandom,OCI_NUM)) && $err == 0){
            $vraiReponseRowRandom = oci_fetch_array($testFR,OCI_NUM );
            $verif=1;
            if(sizeof($reponseRowRandom) == sizeof($vraiReponseRowRandom)){
                for($i=0 ; $i< sizeof($vraiReponseRowRandom)-1 ; $i++){
                    if($reponseRowRandom[$i] != $vraiReponseRowRandom[$i]){
                        $err = -1;
                        break;
                    }    
                }
            }
            else {
                $err = -2;
                break;
            }
        }      
        //echo '.....'.$verif;
        // </editor-fold>
        
        if($err == 0 && $verif==1 ){
            echo "<strong>La réponse à la question " .$numQuestion. " est JUSTE ! </strong>";
            $cmpt++;
        }
        else echo "<strong>La réponse à la question " .$numQuestion. " est FAUSSE ! </strong>";
    }



// Eléments d'authentification LDAP
       /*$ldaprdn  = 'cn=web_bind,OU=DSI,dc=iut,dc=local';     // DN ou RDN LDAP
        $ldappass = 'ldap';  // Mot de passe associé

        // Connexion au serveur LDAP
        $ldapconn = ldap_connect("ldap://192.168.105.5",389)
            or die("Impossible de se connecter au serveur LDAP.");

        if ($ldapconn) {

            // Connexion au serveur LDAP
            $ldapbind = ldap_bind($ldapconn, $ldaprdn, $ldappass);

            // Vérification de l'authentification
            if ($ldapbind) {
               // echo "Connexion LDAP réussie...";
            } else {
               // echo "Connexion LDAP échouée...";
            }

        }

        $dn="OU=ITC,OU=uca,OU=etudiants,OU=utilisateurs,DC=iut,
 DC=local";

        $filter = "cn=".$_SERVER['REMOTE_USER'];
        $sr = ldap_search($ldapconn, $dn, $filter);


        /* echo 'Le résultat de la recherche est ' . $sr . '<br />';

    echo 'Le nombre d\'entrées retournées est ' . ldap_count_entries($ldapconn,$sr) 
         . '<br />';

          echo 'Lecture des entrées ...<br />';
    
    echo 'Données pour ' . $info["count"] . ' entrées:<br />';
for ($i=0; $i<$info["count"]; $i++) {
        echo 'dn est : ' . $info[$i]["dn"] . '<br />';
        echo 'premiere entree cn : ' . $info[$i]["cn"][0] . '<br />';
        echo 'premier date de naissance : ' . $info[$i]["postofficebox"][0] . '<br />';
    }
    $info = ldap_get_entries($ldapconn, $sr);
    $_SESSION["sn"] = $info[0]["sn"][0];

$txt = 'Résultat  de  '.$info[0]["sn"][0] .' : '. $cmpt . '/' . $numQuestion;
echo $txt;



//file_put_contents('resultat.html', ob_get_contents());

$fic = fopen("result.txt", "w");
fwrite($fic, $txt);
fclose($fic);*/

?>
 



