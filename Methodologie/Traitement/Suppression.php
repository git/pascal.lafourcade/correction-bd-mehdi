<?php


require_once('../BDD/SqliteDb.php');
$db = new SqliteDb('o');
        


if (isset($_GET['qcm'])){
    $numQcmSuiv = $_GET['numQuestion']+1;
    
    $sqliteQuery = $db->prepare('SELECT count(*) FROM QcmQuestion WHERE numQcm=?');
    $sqliteQuery->bindParam(1, $_GET['numQcm']);
    $result = $sqliteQuery->execute();
    $nbQuestionsRow =   $result->fetchArray();
    
    $sqliteQuery = $db->prepare('DELETE FROM QcmQuestion WHERE numQuestion= ? AND numQcm=?');
    $sqliteQuery->bindParam(1, $_GET['numQuestion']);
    $sqliteQuery->bindParam(2, $_GET['numQcm']);
    $result = $sqliteQuery->execute();
    
    
    for( $iSuiv = $numQcmSuiv, $j=$_GET['numQuestion'] ; $iSuiv <= $nbQuestionsRow['count(*)'] ; $iSuiv++ , $j++ ){              
        $sqliteQuery = $db->prepare('UPDATE QcmQuestion SET numQuestion=? WHERE numQuestion=? AND numQcm=? ');
        $sqliteQuery->bindParam(1, $j);
        $sqliteQuery->bindParam(2, $iSuiv);
        $sqliteQuery->bindParam(3, $_GET['numQcm']);
        $result = $sqliteQuery->execute();
    }
    
    //même chose pour qcmreponse
    $sqliteQuery = $db->prepare('DELETE FROM QcmReponse WHERE numQuestion= ? AND numQcm = ?');
    $sqliteQuery->bindParam(1, $_GET['numQuestion']);
    $sqliteQuery->bindParam(2, $_GET['numQcm']);
    $result = $sqliteQuery->execute();
    
    
    for( $iSuiv = $numQcmSuiv, $j=$_GET['numQuestion'] ; $iSuiv <= $nbQuestionsRow['count(*)'] ; $iSuiv++ , $j++ ){              
        $sqliteQuery = $db->prepare('UPDATE QcmReponse SET numQuestion=? WHERE numQuestion=? AND numQcm = ? ');
        $sqliteQuery->bindParam(1, $j);
        $sqliteQuery->bindParam(2, $iSuiv);
        $sqliteQuery->bindParam(3, $_GET['numQcm']);
        $result = $sqliteQuery->execute();
    }
     //même chose pour QcmCorrection
    $sqliteQuery = $db->prepare('DELETE FROM QcmCorrection WHERE numQuestion= ? AND numQcm=?');
    $sqliteQuery->bindParam(1, $_GET['numQuestion']);
    $sqliteQuery->bindParam(2, $_GET['numQcm']);
    $result = $sqliteQuery->execute();
    
    
    for( $iSuiv = $numQcmSuiv, $j=$_GET['numQuestion'] ; $iSuiv <= $nbQuestionsRow['count(*)'] ; $iSuiv++ , $j++ ){              
        $sqliteQuery = $db->prepare('UPDATE QcmCorrection SET numQuestion=? WHERE numQuestion=? AND numQcm=? ');
        $sqliteQuery->bindParam(1, $j);
        $sqliteQuery->bindParam(2, $iSuiv);
        $sqliteQuery->bindParam(3, $_GET['numQcm']);
        $result = $sqliteQuery->execute();
    }
    
    if($nbQuestionsRow['count(*)'] == 1){
        $sqliteQuery = $db->prepare('DELETE FROM Qcm WHERE numQcm=?');
        $sqliteQuery->bindParam(1, $_GET['numQcm']);
        $result = $sqliteQuery->execute();
        
        $sqliteQuery = $db->prepare('DELETE FROM QcmSauvegarde WHERE numQcm=?');
        $sqliteQuery->bindParam(1, $_GET['numQcm']);
        $result = $sqliteQuery->execute();
        
        $sqliteQuery = $db->prepare('DELETE FROM Notes WHERE numQcm=?');
        $sqliteQuery->bindParam(1, $_GET['numQcm']);
        $result = $sqliteQuery->execute();
        
        $sqliteQuery = $db->prepare('DELETE FROM NotesParQuestion WHERE numQcm=?');
        $sqliteQuery->bindParam(1, $_GET['numQcm']);
        $result = $sqliteQuery->execute();
    }
    
    
}

else if(isset($_GET['demo'])){
    
    $numDemoSuiv = $_GET['numDemo']+1;
    
    $sqliteQuery = $db->prepare('SELECT count(*) FROM Demonstration WHERE chapitre=?');
    $sqliteQuery->bindParam(1, $_GET['numChapitre']);
    $result = $sqliteQuery->execute();
    $nbQuestionsRow =   $result->fetchArray();
    
    $sqliteQuery = $db->prepare('DELETE FROM Demonstration WHERE numDemo= ? AND chapitre=? ');
    $sqliteQuery->bindParam(1, $_GET['numDemo']);
    $sqliteQuery->bindParam(2, $_GET['numChapitre']);
    $result = $sqliteQuery->execute();
    
    
    for( $iSuiv = $numDemoSuiv, $j=$_GET['numDemo'] ; $iSuiv <= $nbQuestionsRow['count(*)'] ; $iSuiv++ , $j++ ){              
        $sqliteQuery = $db->prepare('UPDATE Demonstration SET numDemo=? WHERE numDemo=? AND chapitre=? ');
        $sqliteQuery->bindParam(1, $j);
        $sqliteQuery->bindParam(2, $iSuiv);
        $sqliteQuery->bindParam(3, $_GET['numChapitre']);
        $result = $sqliteQuery->execute();
    }
}

else {

    $numQuestionSuiv = $_GET['numQuestion']+1;



    $sqliteQuery = $db->prepare('SELECT type FROM Type WHERE numQuestion=? AND numTp = ?');
    $sqliteQuery->bindParam(1, $_GET['numQuestion']);
    $sqliteQuery->bindParam(2, $_GET['numTp']);
    $result = $sqliteQuery->execute();
    $typeRow = $result->fetchArray();

    $sqliteQuery = $db->prepare('DELETE FROM Type WHERE numquestion= ? AND numTp=? ');
    $sqliteQuery->bindParam(1, $_GET['numQuestion']);
    $sqliteQuery->bindParam(2, $_GET['numTp']);
    $result = $sqliteQuery->execute();


    if($typeRow['type'] == 'query'){   
        $sqliteQuery = $db->prepare('SELECT count(*) FROM (SELECT numQuestion FROM Correct WHERE numTp=? UNION SELECT numQuestion FROM FunctionCorrect WHERE numTp=?)');
        $sqliteQuery->bindParam(1, $_GET['numTp']);
        $sqliteQuery->bindParam(2, $_GET['numTp']);
        $result = $sqliteQuery->execute();
        $nbQuestionsRow =   $result->fetchArray();

        $sqliteQuery = $db->prepare('DELETE FROM Correct WHERE numquestion= ? AND numTp=? ');
        $sqliteQuery->bindParam(1, $_GET['numQuestion']);
        $sqliteQuery->bindParam(2, $_GET['numTp']);
        $result = $sqliteQuery->execute();


        for( $iSuiv = $numQuestionSuiv, $j=$_GET['numQuestion'] ; $iSuiv <= $nbQuestionsRow['count(*)'] ; $iSuiv++ , $j++ ){

            $typeQuery = $db->prepare('SELECT type FROM Type WHERE numQuestion = ? AND numTp= ?');
            $typeQuery->bindParam(1, $iSuiv);
            $typeQuery->bindParam(2, $_GET['numTp']);
            $resultType = $typeQuery->execute();
            $typeRow = $resultType->fetchArray();

            if($typeRow['type'] == 'query'){
                $table = 'Correct';
                $type = 'query';
            }
            else{
                $table = 'FunctionCorrect';
                $type = 'functionCorrect';
            }

            $sqliteQuery = $db->prepare('UPDATE '.$table.' SET numQuestion=? WHERE numQuestion=? AND numTp=? ');
            $sqliteQuery->bindParam(1, $j);
            $sqliteQuery->bindParam(2, $iSuiv);
            $sqliteQuery->bindParam(3, $_GET['numTp']);
            $result = $sqliteQuery->execute();

            $sqliteQuery = $db->prepare('UPDATE Type SET numQuestion=? WHERE numQuestion=? AND numTp=? ');
            $sqliteQuery->bindParam(1, $j);
            $sqliteQuery->bindParam(2, $iSuiv);
            $sqliteQuery->bindParam(3, $_GET['numTp']);
            $result = $sqliteQuery->execute();
        }
    }
    else {

        $sqliteQuery = $db->prepare('SELECT count(*) FROM (SELECT numQuestion FROM Correct WHERE numTp=? UNION SELECT numQuestion FROM FunctionCorrect WHERE numTp=?)');
        $sqliteQuery->bindParam(1, $_GET['numTp']);
        $sqliteQuery->bindParam(2, $_GET['numTp']);
        $result = $sqliteQuery->execute();
        $nbQuestionsRow =   $result->fetchArray();

        $sqliteQueryF = $db->prepare('DELETE FROM FunctionCorrect WHERE numquestion= ? AND numTp=?');
        $sqliteQueryF->bindParam(1, $_GET['numQuestion']);
        $sqliteQueryF->bindParam(2, $_GET['numTp']);
        $result = $sqliteQueryF->execute();   



        for( $iSuiv = $numQuestionSuiv, $j=$_GET['numQuestion'] ; $iSuiv <= $nbQuestionsRow['count(*)'] ; $iSuiv++ , $j++ ){

            $typeQuery = $db->prepare('SELECT type FROM Type WHERE numQuestion = ? AND numTp= ?');
            $typeQuery->bindParam(1, $numQuestionSuiv);
            $typeQuery->bindParam(2, $_GET['numTp']);
            $resultType = $typeQuery->execute();
            $typeRow = $resultType->fetchArray();


            if($typeRow['type'] == 'query'){
                $table = 'Correct';
                $type = 'query';
            }
            else{
                $table = 'FunctionCorrect';
                $type = 'functionCorrect';
            }

            $sqliteQuery = $db->prepare('UPDATE '.$table.' SET numQuestion=? WHERE numQuestion=? AND numTp=? ');
            $sqliteQuery->bindParam(1, $j);
            $sqliteQuery->bindParam(2, $iSuiv);
            $sqliteQuery->bindParam(3, $_GET['numTp']);
            $result = $sqliteQuery->execute();

            $sqliteQuery = $db->prepare('UPDATE Type SET numQuestion=? WHERE numQuestion=? AND numTp=? ');
            $sqliteQuery->bindParam(1, $j);
            $sqliteQuery->bindParam(2, $iSuiv);
            $sqliteQuery->bindParam(3, $_GET['numTp']);
            $result = $sqliteQuery->execute();
        }
    }
}// fin else type


