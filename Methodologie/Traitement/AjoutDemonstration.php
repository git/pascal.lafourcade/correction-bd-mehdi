<?php

require_once('../BDD/SqliteDb.php');
require_once('../controleur/Controleur.php');


$db = new SqliteDb('o');


if (empty($_GET['chapitre']) || empty($_GET['bdd'])  || empty($_GET['description']) || empty($_GET['reponse']) ) {
    echo '<strong>ERREUR : Veuillez remplir tous les champs </strong>';
}
    
else if(!is_numeric($_GET['chapitre']))
    echo '<strong>ERREUR : une valeur non numérique à été détectée dans le champ \'Chapitre n°  :\' </strong>';

else {  
    $reponseQuery = $db->prepare('SELECT count(*) FROM demonstration WHERE chapitre=?');
    $reponseQuery->bindParam(1, $_GET['chapitre']);
    $reponseResult = $reponseQuery->execute();
    $reponseRow = $reponseResult->fetchArray();
    $numDemo = $reponseRow['count(*)'] + 1;
    
    if(isset($_GET['modif'])){
        $numDemo = $_GET['numDemo'];
         $db->supprimerDemonstration($_GET['chapitre'], $_GET['numDemo']);
    }


    $isAdd = $db->ajouterDemonstration($_GET['bdd'],$_GET['chapitre'], $numDemo,$_GET['description'], $_GET['reponse']);
    if(!$isAdd){
            echo '<strong>ERREUR : la combinaison (numéro de chapitre, numéro de démonstration) existe déjà dans la bdd</strong>';
        }
    else{
        $trouve=0;
        $file = fopen('..\imports\demonstration\demo_historiqueAuto.txt', "a");
        $ligne = $_GET['bdd'].'**'.$_GET['chapitre'].'**'.$_GET['description'].'**'.$_GET['reponse'];
        $handle = @fopen('..\imports\demonstration\demo_historiqueAuto.txt', "r");
        if ($handle)
        {
            while (!feof($handle))
            {
                $buffer = fgets($handle);
                if(strpos($buffer, $ligne) !== FALSE)
                    $trouve = 1;
            }
            fclose($handle);
        }
        if($trouve==0) fwrite($file, $ligne."\n");
        $trouve = 0;
        fclose($file);
        
        echo '<strong>AJOUT Démonstration n°'.$numDemo.' au chapitre '.$_GET['chapitre'].' avec succès</strong>';
    }

    
    
    
} 
//require_once('index.php');
//header("Location: ../index.php");

