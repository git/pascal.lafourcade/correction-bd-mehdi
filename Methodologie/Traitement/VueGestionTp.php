 
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>

        
    </head>

    <body>
        <form  style="border:1px solid #ccc" method="GET">
            <div class="container">


            <h1>Modifier les dates de tp</h1>
            <hr>
            
            <?php 
                require_once('../BDD/SqliteDb.php');
                
                $db = new SqliteDb('o');
                
                $reponseQuery = $db->prepare('SELECT dateDebut,dateFin FROM dateTp WHERE numTp=?');
                $reponseQuery->bindParam(1,$_GET['tp']);
                $reponseResult = $reponseQuery->execute();
                $reponseRow = $reponseResult->fetchArray();
                
                $anneeDebut = substr($reponseRow['dateDebut'],0,4) ;
                $moisDebut=substr($reponseRow['dateDebut'],4,2);
                $jourDebut=substr($reponseRow['dateDebut'],6,2);
                $dateDebut= $anneeDebut.'-'.$moisDebut.'-'.$jourDebut;
                
                $anneeFin=substr($reponseRow['dateFin'],0,4);
                $moisFin=substr($reponseRow['dateFin'],4,2);
                $jourFin=substr($reponseRow['dateFin'],6,2);
                $dateFin= $anneeFin.'-'.$moisFin.'-'.$jourFin;

            ?>

            <label><b>TP n° : </b></label>
            <input type="text" placeholder="Numéro du TP " id="tp" name='tp' value="<?php echo $_GET['tp'];?> " disabled><br/><br/><br/>
                     
            <label><b>Date début de tp </b></label>
            <input type="date"  id="debut" name='debut' value="<?php echo $dateDebut; ?>" required><br/><br/>
            
            <label><b>Date fin de tp </b></label>
            <input type="date"  id="fin" name='fin' value="<?php echo $dateFin; ?>" required><br/><br/><br/>
          
            <div >
                <input type="button" value="Valider"  onclick="SubmitAjoutDate()" />
            </div>
          </div>
        </form>
        <div id="erreur">
            
        </div>
        
        <form method="get">
            <input id="inp-questions" class="bouton" type="submit" name="action" value="Afficher les questions"> 
        </form>        
    </body>    
       
    <script>
  
        function SubmitAjoutDate() {
                $('#erreur').empty();
                var tp =  $('#tp').val();
                var debut =  $('#debut').val();
                var fin =  $('#fin').val();
                //alert(debut+'____'+fin)
                $.get("Traitement/DateTp.php", { debut : debut, fin : fin , tp :tp },
                function(data) {
                    $('#erreur').html(data);                        

                });             
        }
        
       



    </script>
</html>
