<?php

require_once('../BDD/SqliteDb.php');
$db = new SqliteDb('o');


$numQuestion = $_GET['numQuestion'];
$numTp = $_GET['numTp'];
$numQuestionPrec = $numQuestion-1;
$test = 999;

$Correct = 'Correct';
$function = 'functionCorrect';


if(isset($_GET['des'])){
    $numQuestion += 1;
    $numQuestionPrec = $numQuestion-1;
}

if($numQuestion!=1){
    
    $typeQuery = $db->prepare('SELECT type FROM Type WHERE numQuestion = ? AND numTp= ?');
    $typeQuery->bindParam(1, $numQuestion);
    $typeQuery->bindParam(2, $numTp);
    $resultType = $typeQuery->execute();
    $typeRow = $resultType->fetchArray();
    
    $typeQuery = $db->prepare('SELECT type FROM Type WHERE numQuestion = ? AND numTp= ?');
    $typeQuery->bindParam(1, $numQuestionPrec);
    $typeQuery->bindParam(2, $numTp);
    $resultType = $typeQuery->execute();
    $type2Row = $resultType->fetchArray();
    
    
    //----------
    
    if($typeRow['type'] == 'query'){
        $table = 'Correct';
        $type = 'query';
    }
    else{
        $table = 'FunctionCorrect';
        $type = 'functionCorrect';
    }
    
    
    $sqliteQuery = $db->prepare('UPDATE '.$table.' SET numQuestion=? WHERE numQuestion=? AND numTp=?');   
    $sqliteQuery->bindParam(1, $test);
    $sqliteQuery->bindParam(2, $numQuestion);
    $sqliteQuery->bindParam(3, $numTp);
    $result = $sqliteQuery->execute();
    
    //---------
    if($type2Row['type'] == 'query'){
        $table2 = 'Correct';
        $type2 = 'query';
    }
    else{
        $table2 = 'FunctionCorrect';
        $type2 = 'functionCorrect';
    }
    
    $sqliteQuery = $db->prepare('UPDATE '.$table2.' SET numQuestion=? WHERE numQuestion=? AND numTp=?');
    $sqliteQuery->bindParam(1, $numQuestion);
    $sqliteQuery->bindParam(2, $numQuestionPrec);
    $sqliteQuery->bindParam(3, $numTp);
    $resulti = $sqliteQuery->execute();
    
    $sqliteQuery = $db->prepare('UPDATE Type SET type=? WHERE numQuestion=? AND numTp=?');
    $sqliteQuery->bindParam(1, $type2);
    $sqliteQuery->bindParam(2, $numQuestion);
    $sqliteQuery->bindParam(3, $numTp);
    $resulti = $sqliteQuery->execute();
     
   
    //---------
    
    $sqliteQuery = $db->prepare('UPDATE '.$table.' SET numQuestion=? WHERE numQuestion=? AND numTp=?');

    $sqliteQuery->bindParam(1, $numQuestionPrec);
    $sqliteQuery->bindParam(2, $test);
    $sqliteQuery->bindParam(3, $numTp);
    $resultp = $sqliteQuery->execute();
    
    $sqliteQuery = $db->prepare('UPDATE Type SET type=? WHERE numQuestion=? AND numTp=?');
    $sqliteQuery->bindParam(1, $type);
    $sqliteQuery->bindParam(2, $numQuestionPrec);
    $sqliteQuery->bindParam(3, $numTp);
    $resulti = $sqliteQuery->execute();
    

    
    
}
 
//header("Location: index.php");


