

        <link rel="stylesheet" href="css/VueAdmin.css">


<?php require_once('../BDD/SqliteDb.php');
require_once('../BDD/OracleDb.php');
$db = new SqliteDb('o');




$typeQuery = $db->prepare('SELECT type FROM Type WHERE numQuestion = ? AND numTp= ?');
$typeQuery->bindParam(1, $_GET['numQuestion']);
$typeQuery->bindParam(2, $_GET['numTp']);
$resultType = $typeQuery->execute();
$typeRow = $resultType->fetchArray();

if($typeRow['type']== 'query')
    $stmt = $db->prepare('SELECT question,reponse,points FROM Correct WHERE numQuestion= ? AND numTp = ?');
else if($typeRow['type']== 'functionCorrect')
    $stmt = $db->prepare('SELECT question,reponse,fonctionCorrect,points FROM FunctionCorrect WHERE numQuestion= ? AND numTp = ?');

$stmt->bindParam(1, $_GET['numQuestion']);
$stmt->bindParam(2, $_GET['numTp']);
$resultQuestion = $stmt->execute();
$questionRow = $resultQuestion->fetchArray();




?>
<form  style="border:1px solid #ccc" method="GET" >
  <div class="container">
    <h1>Modifier une question</h1>
    <hr>

    <label><b>TP n° : </b></label>
    <input type="text" placeholder="Numéro du TP" id="tp" value="<?php echo $_GET['numTp']; ?>" required disabled>

    <label><b>Numéro de question : </b></label>
    <input type="text" placeholder="Saisir le numéro de question..." id="num" value="<?php echo $_GET['numQuestion']; ?>" required disabled>

    <label><b>Barème : </b></label>
    <input type="text" placeholder="Barème..." id="points"  value="<?php echo $questionRow['points']; ?>" required>
    
    <label><b>Consigne : </b></label>
    <input type="text" placeholder="Ecrire la consigne..." id="consigne" value="<?php echo $questionRow['question']; ?>" required>

    <label><b>Réponse : </b></label>
    <input type="text" placeholder="SELECT ..." id="reponse" value="<?php echo $questionRow['reponse']; ?>" required>
    
    
    <label> <b>Choisir le type de  la question : </b><br/>
        <input type="radio"  name="type" value="requete" style="margin-bottom:15px" <?php if($typeRow['type']== 'query') echo 'checked'; ?>> requête simple
        <input type="radio"  name="type" value="fonction" id="btn-fonction" style="margin-bottom:15px" <?php if($typeRow['type']== 'functionCorrect') echo 'checked'; ?>> fonction
    </label><br/>
    
    <div id='show-me' style='display:none'>
        <label ><b>Fonction : </b></label>
        <input type="text" placeholder="CREATE OR REPLACE FUNCTION ..." id="fonction" value="<?php if(isset($questionRow['fonctionCorrect'])) echo $questionRow['fonctionCorrect']; ?> " required>
    </div>

    <label><b>Verification avec BDD aléatoire? </b><br/>
        <input type="radio"  name="aleatoire" value="o" style="margin-bottom:15px"  > oui
        <input type="radio"  name="aleatoire" value="n" style="margin-bottom:15px" checked> non
    </label><br/>

    <label><b>Base de données conçernée </b><br/>
        <input type="radio"  name="bdd" value="NBA" style="margin-bottom:15px" checked> NBA
    </label>

    <input type="hidden" id="modif" value="modif" >
    <div class="clearfix">
        <input type="button" value="Modifier"  onclick="SubmitModifierQuestion()" />
    </div>
  </div>
</form>
         <div id="erreur">
            
        </div>
        
        <form method="get" id="frm-tp">
            <input id="inp-tp" class="bouton" type="submit" name="action" value="Afficher les questions" > 
        </form>
         <script>
        
        $(document).ready(function() {
            $('input[type="radio"]').click(function() {
                if($(this).attr('id') == 'btn-fonction') {
                     $('#show-me').show();           
                }

                else {
                     $('#show-me').hide();   
                }
            });
            if ($("#btn-fonction").prop("checked")) {
                 $('#show-me').show();  
            }
        });
        function SubmitModifierQuestion() {
                    //document.write($('#demo'+numQuestion).val());


                    var tp = $('#tp').val() ; 
                    var num = $('#num').val() ; 
                    var points = $('#points').val() ; 
                    var consigne = $('#consigne').val() ; 
                    var reponse = $('#reponse').val() ; 
                    var type = $("input[name='type']:checked").val(); 
                    var aleatoire= $("input[name='aleatoire']:checked").val(); 
                    var bdd = $("input[name='bdd']:checked").val();
                    var fonction = $('#fonction').val() ;    
                    var modif = $('#modif').val();

                   $.get("Traitement/AjoutQuestion.php", { modif: modif, tp: tp,num: num,points: points,consigne: consigne,reponse: reponse,type: type,aleatoire: aleatoire,bdd: bdd, fonction : fonction},
                    function(data) {
                         $('#erreur').html(data);
                         //$('#demoForm')[0].reset();
                         
                      });
        }
        
        function AfficherQuestions(){
            location.reload();
        }



    </script>

