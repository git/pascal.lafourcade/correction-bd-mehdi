<?php
//header('Content-type: text/plain');
require_once('OracleDb.php');

class SqliteDb extends SQLite3
{
    private $conn;
    function __construct($o = 'n')
    {
        $odb = new OracleDb();
        $this->conn = $odb->getConn(); 
        if($o == 'n')
            $this->open('Traitement/test2.db');
        else if($o == 'save'){
            $this->open('../test2.db');
        }
        else $this->open('test2.db');
        //$this->createNotes();
        //$this->createLogin();
       // $this->createTable();
        //$this->createDemonstration();
        //$this->createQCM();
        // $this->createDateTp();
        //$this->creerSauvegardeQcm();
    }
    
    function creerSauvegardeQcm(){
        $this->exec('DROP TABLE QcmSauvegarde');
        $this->exec('CREATE TABLE QcmSauvegarde ( idEtudiant STRING,numQcm NUMBER, numQuestion NUMBER, numReponse NUMBER, UNIQUE(idEtudiant,numQcm,numQuestion,numReponse))');
    }
    
    function createNotes(){
        $this->exec('DROP TABLE Notes');
        //$this->exec('CREATE TABLE Notes ( nomExercice STRING, nomPrenom STRING,numEtudiant NUMBER, note NUMBER, UNIQUE(numEtudiant))');
        $this->exec('CREATE TABLE Notes ( idEtudiant STRING,numQcm NUMBER, note NUMBER, UNIQUE(idEtudiant,numQcm))');
        
        $this->exec('DROP TABLE NotesParQuestion');
        $this->exec('CREATE TABLE NotesParQuestion ( idEtudiant STRING, numQcm NUMBER, numQuestion NUMBER, note NUMBER, UNIQUE(idEtudiant,numQuestion,numQcm))');
        
        
       /* $user = "mehdi";
        $query = $this->prepare('INSERT OR IGNORE INTO Notes VALUES(?,0)');
        $query->bindParam(1, $user);
        $result = $query->execute();
        
        $query = $this->prepare('INSERT OR IGNORE INTO NotesParQuestion(idEtudiant) VALUES(?)');
        $query->bindParam(1, $user);
        $result = $query->execute();*/
    }
    
    function ecrireFichierNotes($numQcm,$save = 'o'){

        // Eléments d'authentification LDAP
        $ldaprdn  = 'cn=web_bind,OU=DSI,dc=iut,dc=local';     // DN ou RDN LDAP
        $ldappass = 'ldap';  // Mot de passe associé

        // Connexion au serveur LDAP
        $ldapconn = ldap_connect("ldap://192.168.105.5",389)
            or die("Impossible de se connecter au serveur LDAP.");

        if ($ldapconn) {

            // Connexion au serveur LDAP
            $ldapbind = ldap_bind($ldapconn, $ldaprdn, $ldappass);

            // Vérification de l'authentification
           /* if ($ldapbind) {
                echo "Connexion LDAP réussie...";
            } else {
                echo "Connexion LDAP échouée...";
            }*/

        }
        $dn="OU=ITC,OU=uca,OU=etudiants,OU=utilisateurs,DC=iut,DC=local";
        $filter = "cn=".$_SERVER['REMOTE_USER'];
        $sr = ldap_search($ldapconn, $dn, $filter);

        $info = ldap_get_entries($ldapconn, $sr);
               
        $query = $this->prepare('SELECT * FROM Qcm WHERE numQcm=?');
        $query->bindParam(1,$numQcm);
        $result = $query->execute();
        $nomResult = $result->fetchArray();
        if(!$nomResult['nom']) exit();
        if($save != 'o'){
            $file = fopen("..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."Notes".DIRECTORY_SEPARATOR."QCM".DIRECTORY_SEPARATOR."notes_".$nomResult['nom'].".txt", "w");
            $fileDetail = fopen("..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."Notes".DIRECTORY_SEPARATOR."QCM".DIRECTORY_SEPARATOR."notesDetaillees_".$nomResult['nom'].".txt", "w");
        }
        else {
             $file = fopen("..".DIRECTORY_SEPARATOR."Notes".DIRECTORY_SEPARATOR."QCM".DIRECTORY_SEPARATOR."notes_".$nomResult['nom'].".txt", "w");
             $fileDetail = fopen("..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."Notes".DIRECTORY_SEPARATOR."QCM".DIRECTORY_SEPARATOR."notesDetaillees_".$nomResult['nom'].".txt", "w");
        }
        $query = $this->prepare('SELECT * FROM Notes WHERE numQcm=?');
        $query->bindParam(1,$numQcm);
        $result = $query->execute();

        $query = $this->prepare('SELECT count(*) FROM QcmQuestion WHERE numQcm=?');
        $query->bindParam(1,$numQcm);
        $resultCount = $query->execute();
        $nbQuestion = $resultCount->fetchArray();

        while($r = $result->fetchArray()){
            $filter = "cn=".$r['idEtudiant'];
            $sr = ldap_search($ldapconn, $dn, $filter);
            $info = ldap_get_entries($ldapconn, $sr);

            $ligne =  $_SERVER['REMOTE_USER'].','.$info[0]["givenname"][0].','.$info[0]["sn"][0].",".$info[0]["postalcode"][0];
            for($i = 1 ; $i<= $nbQuestion['count(*)'] ; $i++){
                $query = $this->prepare('SELECT * FROM NotesParQuestion WHERE numQcm=? AND numQuestion=? AND idEtudiant=?');
                $query->bindParam(1,$numQcm);
                $query->bindParam(2,$i);
                $query->bindParam(3,$r['idEtudiant']);
                $resultNoteQuestions = $query->execute();
                $noteQuestionRow = $resultNoteQuestions->fetchArray();
                $ligne = $ligne.','.$noteQuestionRow['note'];
            }
            $ligne = $ligne.','.$r['note'];
            fwrite($fileDetail,$ligne."\n");
            //echo $info[0]["givenname"][0].','.$info[0]["postalcode"][0].",".$r['note']."\n";
            fwrite($file, $r['idEtudiant'].','.$info[0]["givenname"][0].','.$info[0]["sn"][0].",".$info[0]["postalcode"][0].",".$r['note']."\n");
        }
        fclose($file);         
        
    }
    
    function createTable(){
        //Création De La Table
        $this->exec('DROP TABLE Correct');
        $this->exec('CREATE TABLE Correct ( bddConcernee STRING,numTp NUMBER, numquestion NUMBER,question STRING, reponse STRING,points NUMBER,aleatoire CHAR)');
        
        $this->exec('DROP TABLE FunctionCorrect');
        $this->exec('CREATE TABLE FunctionCorrect (bddConcernee STRING,numTp NUMBER, numquestion NUMBER, question STRING, reponse STRING, fonctionCorrect STRING,testUser STRING, points NUMBER)');
        
        $this->exec('DROP TABLE TriggerCorrect');
        $this->exec('CREATE TABLE TriggerCorrect ( bddConcernee STRING,numTp NUMBER, numquestion NUMBER,question STRING, reponse STRING,fonctionCorrect STRING,testUser STRING)');
        
        $this->exec('DROP TABLE Type');
        $this->exec('CREATE TABLE Type ( numTp NUMBER,numQuestion NUMBER,type STRING)');
        
        $this->exec("INSERT INTO Type VALUES(2,1,'query')");
        $this->exec("INSERT INTO Type VALUES(2,2,'query')");
        $this->exec("INSERT INTO Type VALUES(2,3,'query')");
        $this->exec("INSERT INTO Type VALUES(2,4,'query')");
        $this->exec("INSERT INTO Type VALUES(2,5,'functionCorrect')");
        
        
        $this->createDateTp();

        //Question 1
        $type = 'query';
        $points =1;
        $reponse = 'SELECT count(*) FROM STATS WHERE prenomnoms=\'Kevin Durant\' ';       
        $q = "INSERT INTO Correct VALUES ('NBA',2,1,'Trouver le nombre de matchs joués par Kevin Durant', ? ,?,'o')";
        $stmt = $this->prepare($q);
        $stmt->bindParam(1,$reponse);
        $stmt->bindParam(2,$points);
        $stmt->execute();
        
       //Question 2  
        $type = 'query';    
        $points =1;
        $reponse = ' SELECT prenomnoms,datematch,points,equipeadverse FROM STATS WHERE points = (SELECT max(points) FROM STATS)';
        $q = "INSERT INTO Correct VALUES ('NBA',2,2,'Lister le(s) joueur(s) ayant marqué le plus de points dans la saison en indiquant son nom, la date du match, le nombre de points, l équipe adverse et le nombre de points marqués ', ? ,?,'o')";
        $stmt = $this->prepare($q);
        $stmt->bindParam(1,$reponse);
        $stmt->bindParam(2,$points);
        $stmt->execute();
        
        //Question 3   
        $type = 'query';   
        $points =1;
        $reponse = ' SELECT prenomnoms, COUNT(*) FROM  STATS GROUP BY prenomnoms HAVING count(*) = (SELECT MAX(c) FROM (SELECT COUNT(*) AS c FROM STATS GROUP BY prenomnoms))';
        $q = "INSERT INTO Correct VALUES ('NBA',2,3,'Lister le(s) joueur(s) ayant joué le plus de match pendant la saison', ? ,?,'o')";
        $stmt = $this->prepare($q);
        $stmt->bindParam(1,$reponse);
        $stmt->bindParam(2,$points);
        $stmt->execute();
        
        //Question 4
        $type = 'query';
        $points =1;
        $reponse = 'SELECT nom FROM EQUIPE, STATS WHERE datematch = \'30-OCT-17\'  AND prenomnoms = \'Kevin Durant\'  AND idequipe = CASE WHEN locationjoueur = \'Away\' THEN equipeadverse WHEN locationjoueur = \'Home\' THEN equipejoueur END ';
        $q = "INSERT INTO Correct VALUES ('NBA',2,4,'Déterminer pour Kevin Durant pour le match du 30-oct-17 quelle est l équipe qui joue à domicile', ?,?,'n' )";
        $stmt = $this->prepare($q);
        $stmt->bindParam(1,$reponse);
        $stmt->bindParam(2,$points);
        
        $stmt->execute();
        
        //Question 5
        $this->joueurEtDateRandom($joueurRandom, $dateRandom);
        $type = 'fonction';
        $fonctionCorrect = 'CREATE OR REPLACE FUNCTION fpointsmarques(date_match IN VARCHAR2, joueur IN VARCHAR2 ) RETURN NUMBER IS nbPoints NUMBER ; BEGIN SELECT  points INTO nbPoints FROM STATS WHERE datematch = date_match AND prenomnoms=joueur; RETURN nbPoints; END;' ;
        $reponse = 'SELECT fpointsmarques(\''.$dateRandom.'\', \''.$joueurRandom.'\') FROM DUAL';       
        $testU =    'SELECT pointsmarques(\''.$dateRandom.'\', \''.$joueurRandom.'\') FROM DUAL' ;        
        $points =1;
        $q = "INSERT INTO FunctionCorrect VALUES ('NBA',2,5,'Ecrire une fonction pointsmarques qui détermine pour une date de match et un joueur donnés quelle est le nombre de points marqués par ce joueur', ?,?,?,? )";
        $stmt = $this->prepare($q);
        $stmt->bindParam(1,$reponse);
        $stmt->bindParam(2,$fonctionCorrect);
        $stmt->bindParam(3,$testU);
        $stmt->bindParam(4,$points);
        $stmt->execute();
        
      /*   //Question 9
        $type = 'tablemodification';
            $fonctionCorrect = 'IDJOUEUR';
            $testU = null;
            $testP = 'SELECT data_type FROM all_TAB_COLUMNS WHERE table_name = \'JOUEUR\' '; //pour vérifier l
            $reponse = 'SELECT  column_name FROM all_TAB_COLUMNS WHERE table_name = \'TEST\' ';
            $q = "INSERT INTO Correct VALUES (6,'Modifier la table JOUEUR pour ajouter un champ idjoueur', ?,?,?,? )";
        $stmt = $this->prepare($q);
        $stmt->bindParam(1,$reponse);
        $stmt->bindParam(2,$type);
        $stmt->bindParam(3,$fonctionCorrect);
        $stmt->bindParam(4,$testU);
        
        //Question 10
        $type = 'query';
        $fonctionCorrect = null;
        $testU = null;
        $testP = null;
        $reponse = 'SELECT id_joueur FROM STATS WHERE table_name = \'TEST\' ';
        $q = "INSERT INTO Correct VALUES (7,'Modifier la table JOUEUR pour ajouter un champ idjoueur', ?,?,?,? )";
        $stmt = $this->prepare($q);
        $stmt->bindParam(1,$reponse);
        $stmt->bindParam(2,$type);
        $stmt->bindParam(3,$fonctionCorrect);
        $stmt->bindParam(4,$testU);
        $stmt->execute();*/
        
        //QUESITION 1 TP2
        $type='trigger';
        $fonctionCorrect = 'CREATE OR REPLACE TRIGGER supprJ BEFORE DELETE ON joueur FOR EACH ROW DECLARE duser varchar2(30); ddate date; BEGIN	SELECT user,sysdate INTO duser,ddate FROM DUAL;	INSERT INTO TRACE VALUES(ddate, duser,\'Suppression de \' || :old.prenomnomj); END;' ;
        //
        
    }
    
    function createDemonstration(){
        $this->exec('DROP TABLE Demonstration');
        $this->exec('CREATE TABLE Demonstration ( bddconcernee STRING ,chapitre NUMBER, numDemo NUMBER, description STRING,reponse STRING)');
        
        $this->exec("INSERT INTO Demonstration VALUES('NBA',1,1,'Exemple de requête pour selectionner les 2 premières lignes de la table JOUEUR','SELECT * FROM JOUEUR WHERE rownum<3')");
        $this->exec("INSERT INTO Demonstration VALUES('NBA',1,2,'Exemple de requête pour selectionner les 2 premières lignes de la table STATS','SELECT * FROM STATS WHERE rownum<3')");
        $this->exec("INSERT INTO Demonstration VALUES('NBA',2,1,'Exemple de requête pour selectionner les 2 premières lignes de la table GAME','SELECT * FROM GAME WHERE rownum<3')");
    }
    
    function createQCM(){
        $this->exec('DROP TABLE Qcm');
        $this->exec('CREATE TABLE Qcm ( numQcm NUMBER, nom STRING,type STRING,introduction STRING)');
        
        $this->exec('DROP TABLE QcmQuestion');
        $this->exec('CREATE TABLE QcmQuestion ( numQuestion NUMBER,numQcm NUMBER, question STRING,bareme NUMBER)');  
        
        $this->exec('DROP TABLE QcmReponse');
        $this->exec('CREATE TABLE QcmReponse ( numQcm NUMBER, numReponse NUMBER, reponse STRING, numQuestion NUMBER, points NUMBER)');
        
        $this->exec('DROP TABLE QcmCorrection');
        $this->exec('CREATE TABLE QcmCorrection (numQcm NUMBER, numQuestion NUMBER, numReponse NUMBER)');
        
        //------QCM sql --------------//
        /*$this->exec("INSERT INTO Qcm VALUES(1,'sql','vraifaux','intro sql')");
        
        //Question 1
        $this->exec("INSERT INTO QcmQuestion VALUES(1,1,'Ceci est la question 1',10)");
        
        $this->exec("INSERT INTO QcmReponse(numQcm,numReponse,reponse,numQuestion) VALUES(1,1,'Ceci est la reponse 1 de la question 1(fausse)',1)");
        $this->exec("INSERT INTO QcmReponse(numQcm,numReponse,reponse,numQuestion) VALUES(1,2,'Ceci est la reponse 2 de la question 1(Vraie)',1)");
        $this->exec("INSERT INTO QcmReponse(numQcm,numReponse,reponse,numQuestion) VALUES(1,3,'Ceci est la reponse 3 de la question 1(Vraie)',1)");
         
        $this->exec("INSERT INTO QcmCorrection VALUES(1,1,2)");
        $this->exec("INSERT INTO QcmCorrection VALUES(1,1,3)");
        
        //Question 2
        $this->exec("INSERT INTO QcmQuestion VALUES(2,1,'Ceci est la question 2',9)");
        
        $this->exec("INSERT INTO QcmReponse(numQcm,numReponse,reponse,numQuestion) VALUES(1,1,'Ceci est la reponse 1 de la question 2(Vraie)',2)");
        $this->exec("INSERT INTO QcmReponse(numQcm,numReponse,reponse,numQuestion) VALUES(1,2,'Ceci est la reponse 2 de la question 2(fausse)',2)");
        $this->exec("INSERT INTO QcmReponse(numQcm,numReponse,reponse,numQuestion) VALUES(1,3,'Ceci est la reponse 3 de la question 2(fausse)',2)");
        $this->exec("INSERT INTO QcmReponse(numQcm,numReponse,reponse,numQuestion) VALUES(1,4,'Ceci est la reponse 4 de la question 2(fausse)',2)");
         
        $this->exec("INSERT INTO QcmCorrection VALUES(1,2,1)");
        
         //------QCM EPSWORTH --------------//
        $this->exec("INSERT INTO Qcm VALUES(2,'EPSWORTH','test','intro epsworth')");
        
        //Question 1
        $this->exec("INSERT INTO QcmQuestion(numQuestion,numQcm,question) VALUES(1,2,'Ceci est la question 1')");
        
        $this->exec("INSERT INTO QcmReponse VALUES(2,1,'Ceci est la reponse 1 de la question 1(5)',1,5)");
        $this->exec("INSERT INTO QcmReponse VALUES(2,2,'Ceci est la reponse 2 de la question 1(11)',1,11)");
        $this->exec("INSERT INTO QcmReponse VALUES(2,3,'Ceci est la reponse 3 de la question 1(12)',1,12)");
         
        
        //Question 2
        $this->exec("INSERT INTO QcmQuestion(numQuestion,numQcm,question) VALUES(2,2,'Ceci est la question 2')");
        
        $this->exec("INSERT INTO QcmReponse VALUES(2,1,'Ceci est la reponse 1 de la question 2(5)',2,5)");
        $this->exec("INSERT INTO QcmReponse VALUES(2,2,'Ceci est la reponse 2 de la question 2(11)',2,11)");
        $this->exec("INSERT INTO QcmReponse VALUES(2,3,'Ceci est la reponse 3 de la question 2(12)',2,12)");*/
        
    }
    
    function createLogin(){
        $mdp = password_hash('mdptest', PASSWORD_DEFAULT);
        $username = 'test';
        $this->exec('DROP TABLE login');
        $this->exec('CREATE TABLE login ( username STRING, password STRING)');
        
        $stmt = $this->prepare("INSERT INTO login VALUES(? , ?)");
        $stmt->bindParam(1, $username);
        $stmt->bindParam(2, $mdp);
        $stmt->execute();        
    }
    
    function createDateTp(){
        $this->exec('DROP TABLE dateTp');
        $this->exec('CREATE TABLE dateTp ( numTp NUMBER, dateDebut STRING, dateFin STRING)');
        
        $this->exec("INSERT INTO dateTp VALUES(2,'20200101','20200814')");
    }

    function joueurEtDateRandom(&$nomRandom, &$dateRandom){
        
        $listeJoueurQuery = oci_parse($this->conn, 'SELECT prenomnoms,datematch FROM STATS INTERSECT SELECT prenomnoms,datematch FROM randomstats ');
        oci_execute($listeJoueurQuery);
        $listeJoueurRows = oci_fetch_all($listeJoueurQuery, $listeJoueurArray);  
        //print_r($listeJoueurArray);
        $rand_keys = array_rand($listeJoueurArray['PRENOMNOMS'], 1);
        $nomRandom =  $listeJoueurArray['PRENOMNOMS'][$rand_keys];  
        $dateRandom = $listeJoueurArray['DATEMATCH'][$rand_keys];
    }
    
    function joueurEtDateRandom2(&$nomRandom, &$dateRandom){
        $listeJoueurQuery = oci_parse($this->conn, 'SELECT prenomnoms,datematch FROM STATS');
        oci_execute($listeJoueurQuery);
        $listeJoueurRows = oci_fetch_all($listeJoueurQuery, $listeJoueurArray);  
        //print_r($listeJoueurArray);
        $rand_keys = array_rand($listeJoueurArray['PRENOMNOMS'], 1);
        $nomRandom =  $listeJoueurArray['PRENOMNOMS'][$rand_keys];  
        $dateRandom = $listeJoueurArray['DATEMATCH'][$rand_keys];
    }

    function ajouterRequete($contexte,$tp,$numQuestion,$consigne,$requete,$points){               
        if($this->verificationExistenceQuestion($tp, $numQuestion)) return false;
        $qt = "INSERT INTO Type VALUES(?,?,'query')";
        $prp=$this->prepare($qt);
        $prp->bindParam(1, $tp);
        $prp->bindParam(2, $numQuestion);
        $prp->execute();
        
        $q = "INSERT INTO Correct VALUES (?,?,?,?,? ,?,?)";
        $stmt = $this->prepare($q);
        $stmt->bindParam(1,$contexte);
        $stmt->bindParam(2,$tp);
        $stmt->bindParam(3,$numQuestion);
        $stmt->bindParam(4,$consigne);
        $stmt->bindParam(5,$requete);
        $stmt->bindParam(6,$points);
        $stmt->bindParam(7,$contexte);

        $stmt->execute();
        return true;
    }
    
    function ajouterFonction($contexte,$tp,$numQuestion,$consigne,$requete,$fonction,$requeteUser,$points){               
        if($this->verificationExistenceQuestion($tp, $numQuestion)) return false;
        $qt = "INSERT INTO Type VALUES(?,?,'functionCorrect')";
        $prp=$this->prepare($qt);
        $prp->bindParam(1, $tp);
        $prp->bindParam(2, $numQuestion);
        $prp->execute();
        $q = "INSERT INTO FunctionCorrect VALUES (?,?,?,?,?,?,? ,?)";
        $stmt = $this->prepare($q);
        $stmt->bindParam(1,$contexte);
        $stmt->bindParam(2,$tp);
        $stmt->bindParam(3,$numQuestion);
        $stmt->bindParam(4,$consigne);
        $stmt->bindParam(5,$requete);
        $stmt->bindParam(6,$fonction);
        $stmt->bindParam(7,$requeteUser);
        $stmt->bindParam(8,$points);
        $stmt->execute();
        return true;
    }
    
    function ajouterQCM($numQcm,$numQuestion,$consigne,$choix,$reponses,$bareme){               
        if($this->verificationExistenceQCM($numQcm,$numQuestion)) return false;           
        $qt = "INSERT INTO QcmQuestion VALUES(?,?,?,?)";
        $prp=$this->prepare($qt);
        $prp->bindParam(1, $numQuestion);
        $prp->bindParam(2, $numQcm);
        $prp->bindParam(3, $consigne);
        $prp->bindParam(4, $bareme);
        $prp->execute();
        
        $i=1;
        foreach($choix as $c){
            $q = "INSERT INTO QcmReponse(numQcm,numReponse,reponse,numQuestion) VALUES (?,?,?,?)";
            $stmt = $this->prepare($q);
            $stmt->bindParam(1,$numQcm);
            $stmt->bindParam(2,$i);
            $stmt->bindParam(3,$c);
            $stmt->bindParam(4,$numQuestion);
            $stmt->execute();
            $i++;
        }
     
        foreach($reponses as $numReponse){
            $qt = "INSERT INTO QcmCorrection VALUES(?,?,?)";
            $prp=$this->prepare($qt);
            $prp->bindParam(1,$numQcm);
            $prp->bindParam(2, $numQuestion);
            $prp->bindParam(3, $numReponse);
            $prp->execute();
        }
        return true;
    }
    
    function ajouterQCMTest($numQcm,$numQuestion,$consigne,$choixEtPoints){
       	if($this->verificationExistenceQCM($numQcm,$numQuestion)) return false;            
        $qt = "INSERT INTO QcmQuestion(numQuestion,numQcm,question) VALUES(?,?,?)";
        $prp=$this->prepare($qt);
        $prp->bindParam(1, $numQuestion);
        $prp->bindParam(2, $numQcm);
        $prp->bindParam(3, $consigne);
        $prp->execute();
        $i=1;
        foreach($choixEtPoints as $c ){
            
            $res = explode("---",$c);
            $q = "INSERT INTO QcmReponse VALUES (?,?,?,?,?)";
            $stmt = $this->prepare($q);
            $stmt->bindParam(1,$numQcm);
            $stmt->bindParam(2,$i);
            $stmt->bindParam(3,$res[0]);
            $stmt->bindParam(4,$numQuestion);
            $stmt->bindParam(5,$res[1]);
            $stmt->execute();
            $i++;
        }
        return true;

    }
    function verificationExistenceQCM($numQcm,$numQuestion){
        $q = "SELECT count(*) FROM QcmQuestion WHERE  numQuestion = ? AND numQcm=?";
        $prp=$this->prepare($q);      
        $prp->bindParam(1, $numQuestion);
        $prp->bindParam(2, $numQcm);        
        $result = $prp->execute();
        $row = $result->fetchArray();
        if($row['count(*)'] == 0) return false;
        return true;
    }
    
    function verificationExistenceQuestion($tp,$numQuestion){
        $q = "SELECT count(*) FROM Type WHERE numTp = ? AND numQuestion = ? ";
        $prp=$this->prepare($q);      
        $prp->bindParam(1, $tp);
        $prp->bindParam(2, $numQuestion);
        $result = $prp->execute();
        $row = $result->fetchArray();
        if($row['count(*)'] == 0){
            $q = "SELECT count(*) FROM Type WHERE numTp = ?";
            $prp=$this->prepare($q);      
            $prp->bindParam(1, $tp);
            $result = $prp->execute();
            $row = $result->fetchArray();
            if($row['count(*)'] == 0){
                date_default_timezone_set('Europe/Paris');
                $dateD = date('Ymd');
                $dateF = date('Ymd',strtotime('+9 months'));
                $q = "INSERT INTO DateTp VALUES(?,?,?)";
                $prp=$this->prepare($q);      
                $prp->bindParam(1, $tp);
                $prp->bindParam(2, $dateD);
                $prp->bindParam(3, $dateF);
                $result = $prp->execute();
            }
            return false;
        }
        return true;
    }
            
    function ajouterDemonstration($contexte,$chapitre,$num,$intitule,$requete){
        if($this->verificationExistenceDemo($chapitre, $num)) return false;
        $q = "INSERT INTO Demonstration VALUES (?,?,?,?,?)";
        $stmt = $this->prepare($q);
        $stmt->bindParam(1,$contexte);
        $stmt->bindParam(2,$chapitre);
        $stmt->bindParam(3,$num);
        $stmt->bindParam(4,$intitule);
        $stmt->bindParam(5,$requete);      
        $stmt->execute();
        
        return true;
        
    }
    
    function verificationExistenceDemo($chapitre,$numDemo){
        $q = "SELECT count(*) FROM Demonstration WHERE chapitre = ? AND numDemo = ? ";
        $prp=$this->prepare($q);      
        $prp->bindParam(1, $chapitre);
        $prp->bindParam(2, $numDemo);
        $result = $prp->execute();
        $row = $result->fetchArray();
        if($row['count(*)'] == 0) return false;
        return true;
    }
    
    function supprimerRequete($numQuestion,$numTp){
        $qt = "DELETE FROM Type WHERE numTp=? AND numQuestion=? ";
        $prp=$this->prepare($qt);
        $prp->bindParam(1, $numTp);
        $prp->bindParam(2, $numQuestion);
        $prp->execute();
        
        $qt = "DELETE FROM Correct WHERE numTp=? AND numQuestion=? ";
        $prp=$this->prepare($qt);
        $prp->bindParam(1, $numTp);
        $prp->bindParam(2, $numQuestion);
        $prp->execute();
    }

    function supprimerFonction($numQuestion,$numTp){
        $qt = "DELETE FROM Type WHERE numTp=? AND numQuestion=? ";
        $prp=$this->prepare($qt);
        $prp->bindParam(1, $numTp);
        $prp->bindParam(2, $numQuestion);
        $prp->execute();
        
        $qt = "DELETE FROM FunctionCorrect WHERE numTp=? AND numQuestion=? ";
        $prp=$this->prepare($qt);
        $prp->bindParam(1, $numTp);
        $prp->bindParam(2, $numQuestion);
        $prp->execute();
    }

    function supprimerDemonstration($chapitre, $numDemo){
        $qt = "DELETE FROM Demonstration WHERE chapitre=? AND numDemo=? ";
        $prp=$this->prepare($qt);
        $prp->bindParam(1, $chapitre);
        $prp->bindParam(2, $numDemo);
        $prp->execute();
    }
    
    function supprimerQCM($numQcm,$numQuestion,$type){
        $qt = "DELETE FROM QcmQuestion WHERE numQuestion=? AND numQcm = ?";
        $prp=$this->prepare($qt);
        $prp->bindParam(1, $numQuestion);
        $prp->bindParam(2, $numQcm);
        $prp->execute();
        
        $qt = "DELETE FROM QcmReponse WHERE numQuestion=? AND numQcm = ?";
        $prp=$this->prepare($qt);
        $prp->bindParam(1, $numQuestion);
        $prp->bindParam(2, $numQcm);
        $prp->execute();
        
        if($type == 'test') return;
        $qt = "DELETE FROM QcmCorrection WHERE numQuestion=? AND numQcm = ?";
        $prp=$this->prepare($qt);
        $prp->bindParam(1, $numQuestion);
        $prp->bindParam(2, $numQcm);
        $prp->execute();
    }
}

