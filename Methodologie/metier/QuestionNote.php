<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QuestionNote
 *
 * @author Mehdi
 */
class QuestionNote extends Question {
    private $bareme;
    
    function __construct($numTp, $numQuestion, $question, $reponse,$bareme) {
        parent::__construct($numTp, $numQuestion, $question, $reponse);
        $this->bareme = $bareme;
    }    
    
    public function getBareme(){
        return $this->bareme;
    }
}
