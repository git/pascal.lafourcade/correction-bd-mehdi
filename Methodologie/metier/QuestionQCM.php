<?php

class QuestionQCM {
    
    
    private $numQuestion;
    private $question;
    private $bareme;
    private $reponse = array();
    
    function __construct($numQuestion,$question,$reponse,$bareme){
        $this->numQuestion = $numQuestion;
        $this->question = $question;
        $this->reponse = $reponse;
        $this->bareme= $bareme;
    }
    
    // Getters
    function getNumQuestion() {
        return $this->numQuestion;
    }

    function getQuestion() {
        return $this->question;
    }

    function getReponse() {
        return $this->reponse;
    }
    function getBareme() {
        return $this->bareme;
    }

    
    // Setters
    function setNumQuestion($numQuestion) {
        $this->numQuestion = $numQuestion;
    }

    function setQuestion($question) {
        $this->question = $question;
    }

    function setReponse($reponse) {
        $this->reponse = $reponse;
    }
    function setBareme($bareme) {
        $this->bareme = $bareme;
    }



    
}



