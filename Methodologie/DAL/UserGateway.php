<?php

    class UserGateway {
        private $db;
        
        public function __construct(SqliteDb $sd) {
            $this->db = $sd;
        }
        
        public function getPassword($identifiant){
            $q = "SELECT password FROM login WHERE username= ";
            $stmt = $this->db->prepare("SELECT * FROM login WHERE username= ?");
            $stmt->bindParam(1, $identifiant);
            $result = $stmt->execute();        
            $passArray = $result->fetcharray();            
            return $passArray['password'];
        }
    }

